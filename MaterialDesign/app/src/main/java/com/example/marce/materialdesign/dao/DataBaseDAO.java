package com.example.marce.materialdesign.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.marce.materialdesign.model.Aula;

import java.util.ArrayList;
import java.util.List;

public class DataBaseDAO {

    private AulaDBHelper aulaHelper;
    private SQLiteDatabase bd;

    public DataBaseDAO(Context context) {
        aulaHelper = new AulaDBHelper(context);
    }

    /**
     *
     * @param aula
     */
    public void insert(Aula aula) {
        try {
            bd = aulaHelper.getWritableDatabase();
            ContentValues valorProfessor = new ContentValues();
            ContentValues valorCurso = new ContentValues();
            ContentValues valorTurma = new ContentValues();
            ContentValues valorDia = new ContentValues();
            ContentValues valorHora = new ContentValues();
            ContentValues valorSala = new ContentValues();
            ContentValues valorAula = new ContentValues();
            ContentValues valorDisciplina = new ContentValues();
            ContentValues valorAluno = new ContentValues();

            valorProfessor.put(DataBaseContract.Professor.COLUNA_PROFESSOR, aula.getProfessor());
            long idProfessor = bd.insert(DataBaseContract.Professor.NOME_TABELA, null, valorProfessor);

            valorCurso.put(DataBaseContract.Curso.COLUNA_CURSO, aula.getCurso());
            long idCurso = bd.insert(DataBaseContract.Curso.NOME_TABELA, null, valorCurso);

            valorTurma.put(DataBaseContract.Turma.COLUNA_TURMA, aula.getTurma());
            long idTurma = bd.insert(DataBaseContract.Turma.NOME_TABELA, null, valorTurma);

            valorDia.put(DataBaseContract.Dia.COLUNA_DIA, aula.getDiaSemana());
            long idDia = bd.insert(DataBaseContract.Dia.NOME_TABELA, null, valorDia);

            valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_INICIO, aula.getHora());
            valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_FINAL, aula.getHoraTermino());
            long idHora = bd.insert(DataBaseContract.Horario.NOME_TABELA, null, valorHora);

            valorSala.put(DataBaseContract.Sala.COLUNA_SALA, aula.getSala());
            long idSala = bd.insert(DataBaseContract.Sala.NOME_TABELA, null, valorSala);


            valorAula.put(DataBaseContract.Aula.COLUNA_FK_DIA, idDia);
            valorAula.put(DataBaseContract.Aula.COLUNA_FK_HORARIO, idHora);
            valorAula.put(DataBaseContract.Aula.COLUNA_FK_SALA, idSala);
            long idAula = bd.insert(DataBaseContract.Aula.NOME_TABELA, null, valorAula);

            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA, aula.getNome());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_SEMESTRE, aula.getSemestre());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO, aula.getTempoNotificacao());
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_CURSO, idCurso);
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR, idProfessor);
            valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_FK_TURMA, idTurma);
            long idDisciplina = bd.insert(DataBaseContract.Disciplina.NOME_TABELA, null, valorDisciplina);

            valorAluno.put(DataBaseContract.Aula_has_disciplinas.FK_AULA, idAula);
            valorAluno.put(DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA, idDisciplina);
            bd.insert(DataBaseContract.Aula_has_disciplinas.NOME_TABELA, null, valorAluno);

            bd.close();
        } catch (Exception e) {
            Log.w("FALHA", "NÃO DEU CERTO!");
            bd.close();

        }

    }

    /**
     *
     * @return Aula
     */

    public List<Aula> select(){

        List<Aula> aulas = new ArrayList<Aula>();
        Cursor aulasBd;
        bd = aulaHelper.getReadableDatabase();

        //Por enquanto esse sql pode ficar aqui dentro, mas quando tiver as tabelas do aluno ae isso vai ter q sair
        String sql = "Select " +DataBaseContract.Aula_has_disciplinas.NOME_TABELA+"."+DataBaseContract.Aula_has_disciplinas._ID+" , "
                + DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA + ", "
                + DataBaseContract.Professor.COLUNA_PROFESSOR + ", "
                + DataBaseContract.Turma.COLUNA_TURMA + ", "
                + DataBaseContract.Curso.COLUNA_CURSO + ", "
                + DataBaseContract.Disciplina.COLUNA_SEMESTRE + ", "
                + DataBaseContract.Disciplina.COLUNA_NOTIFICACAO + ", "
                + DataBaseContract.Dia.COLUNA_DIA + ", "
                + DataBaseContract.Horario.COLUNA_HORARIO_INICIO + ", "
                + DataBaseContract.Horario.COLUNA_HORARIO_FINAL + ", "
                + DataBaseContract.Sala.COLUNA_SALA+" from "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA+
                " inner join "+DataBaseContract.Disciplina.NOME_TABELA+" on "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA+"."+DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA+" = "+DataBaseContract.Disciplina.NOME_TABELA+"."+DataBaseContract.Disciplina._ID+
                " inner join "+DataBaseContract.Professor.NOME_TABELA+ " on "+DataBaseContract.Disciplina.NOME_TABELA+"."+DataBaseContract.Disciplina.COLUNA_FK_PROFESSOR+" = "+DataBaseContract.Professor.NOME_TABELA+"."+DataBaseContract.Professor._ID+
                " inner join "+DataBaseContract.Turma.NOME_TABELA+" on " +DataBaseContract.Disciplina.NOME_TABELA+"."+DataBaseContract.Disciplina.COLUNA_FK_TURMA+" = "+DataBaseContract.Turma.NOME_TABELA+"."+DataBaseContract.Turma._ID+
                " inner join "+DataBaseContract.Curso.NOME_TABELA+" on "+DataBaseContract.Disciplina.NOME_TABELA+"."+DataBaseContract.Disciplina.COLUNA_FK_CURSO+" = "+ DataBaseContract.Curso.NOME_TABELA+"."+DataBaseContract.Turma._ID+
                " inner join "+DataBaseContract.Aula.NOME_TABELA+" on "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA+"."+DataBaseContract.Aula_has_disciplinas.FK_AULA+" = "+DataBaseContract.Aula.NOME_TABELA+"."+DataBaseContract.Aula._ID+
                " inner join "+DataBaseContract.Dia.NOME_TABELA+" on "+DataBaseContract.Aula.NOME_TABELA+"."+DataBaseContract.Aula.COLUNA_FK_DIA+" = "+DataBaseContract.Dia.NOME_TABELA+"."+DataBaseContract.Dia._ID+
                " inner join "+DataBaseContract.Horario.NOME_TABELA+" on "+DataBaseContract.Aula.NOME_TABELA+"."+DataBaseContract.Aula.COLUNA_FK_HORARIO+" = "+DataBaseContract.Horario.NOME_TABELA+"."+DataBaseContract.Horario._ID+
                " inner join "+DataBaseContract.Sala.NOME_TABELA+" on "+DataBaseContract.Aula.NOME_TABELA+"."+DataBaseContract.Aula.COLUNA_FK_SALA+" = "+DataBaseContract.Sala.NOME_TABELA+"."+DataBaseContract.Sala._ID+";";

        aulasBd = bd.rawQuery(sql, null);

        //laço para percorer a tabela que vai conter todas as informações nescessárias
        if(aulasBd.moveToFirst()){
            do {

                Aula aula = new Aula();
                aula.setId(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Aula_has_disciplinas._ID)));
                aula.setNome(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA)));
                aula.setTempoNotificacao(aulasBd.getInt(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO)));
                aula.setCurso(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Curso.COLUNA_CURSO)));
                aula.setDiaSemana(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Dia.COLUNA_DIA)));
                aula.setHora(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_INICIO)));
                aula.setHoraTermino(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Horario.COLUNA_HORARIO_FINAL)));
                aula.setProfessor(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Professor.COLUNA_PROFESSOR)));
                aula.setSala(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Sala.COLUNA_SALA)));
                aula.setTurma(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Turma.COLUNA_TURMA)));
                aula.setSemestre(aulasBd.getString(aulasBd.getColumnIndex(DataBaseContract.Disciplina.COLUNA_SEMESTRE)));

                aulas.add(aula);

            }while (aulasBd.moveToNext());
        }
        bd.close();
        return aulas;
    }

    /**
     *
     * @param aula
     */

    public void delete(Aula aula) {

        bd = aulaHelper.getReadableDatabase();

        String whereAulaResDisciplina = DataBaseContract.Aula_has_disciplinas._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Aula_has_disciplinas.NOME_TABELA,whereAulaResDisciplina,null);

        String whereDisciplina = DataBaseContract.Disciplina._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Disciplina.NOME_TABELA,whereDisciplina,null);

        String whereCurso = DataBaseContract.Curso._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Curso.NOME_TABELA,whereCurso,null);

        String whereProfessor = DataBaseContract.Professor._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Professor.NOME_TABELA,whereProfessor,null);

        String whereTurma = DataBaseContract.Turma._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Turma.NOME_TABELA,whereTurma,null);

        String whereAula = DataBaseContract.Aula._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Aula.NOME_TABELA,whereAula,null);

        String whereDia = DataBaseContract.Dia._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Dia.NOME_TABELA,whereDia,null);

        String whereHorario = DataBaseContract.Horario._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Horario.NOME_TABELA,whereHorario,null);

        String whereSala = DataBaseContract.Sala._ID + "=" + aula.getId();
        bd.delete(DataBaseContract.Sala.NOME_TABELA,whereSala,null);

        bd.close();
    }

    /**
     *
     * @param aula
     */
    public void deleteAll(Aula aula){

        bd = aulaHelper.getReadableDatabase();

        Cursor aulasDelete;

        String sqlBusca="select "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA+"."+DataBaseContract.Aula_has_disciplinas._ID +" from "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA
                +" inner join "+DataBaseContract.Disciplina.NOME_TABELA+" on "+DataBaseContract.Aula_has_disciplinas.NOME_TABELA+"." +
                ""+DataBaseContract.Aula_has_disciplinas.FK_DISCIPLINA+" = "+DataBaseContract.Disciplina.NOME_TABELA+"." +
                ""+DataBaseContract.Disciplina._ID
                +" where "+DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA+" = '"+ aula.getNome()+"';";


        aulasDelete = bd.rawQuery(sqlBusca, null);

        //laço para percorer a tabela que vai conter todas as informações nescessárias
        if(aulasDelete.moveToFirst()){
            do {
                String where = DataBaseContract.Aula_has_disciplinas._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Aula_has_disciplinas.NOME_TABELA,where,null);

                String whereDisciplina = DataBaseContract.Disciplina._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Disciplina.NOME_TABELA,whereDisciplina,null);

                String whereCurso = DataBaseContract.Curso._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Curso.NOME_TABELA,whereCurso,null);

                String whereProfessor = DataBaseContract.Professor._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Professor.NOME_TABELA,whereProfessor,null);

                String whereTurma = DataBaseContract.Turma._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Turma.NOME_TABELA,whereTurma,null);

                String whereAula = DataBaseContract.Aula._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Aula.NOME_TABELA,whereAula,null);

                String whereDia = DataBaseContract.Dia._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Dia.NOME_TABELA,whereDia,null);

                String whereHorario = DataBaseContract.Horario._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Horario.NOME_TABELA,whereHorario,null);

                String whereSala = DataBaseContract.Sala._ID + "=" + aulasDelete.getString(0);
                bd.delete(DataBaseContract.Sala.NOME_TABELA,whereSala,null);
            }while (aulasDelete.moveToNext());
        }

        bd.close();
    }



    /**
     *
     * @param aulaNova
     */

    public void update(Aula aulaNova) {
        bd = aulaHelper.getWritableDatabase();

        ContentValues valorProfessor = new ContentValues();
        ContentValues valorCurso = new ContentValues();
        ContentValues valorTurma = new ContentValues();
        ContentValues valorDia = new ContentValues();
        ContentValues valorHora = new ContentValues();
        ContentValues valorSala = new ContentValues();
        ContentValues valorDisciplina = new ContentValues();

        String whereDiciplina = DataBaseContract.Disciplina._ID+"="+aulaNova.getId();
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOME_DISCIPLINA, aulaNova.getNome());
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_NOTIFICACAO, aulaNova.getTempoNotificacao());
        valorDisciplina.put(DataBaseContract.Disciplina.COLUNA_SEMESTRE, aulaNova.getSemestre());
        bd.update(DataBaseContract.Disciplina.NOME_TABELA,valorDisciplina,whereDiciplina,null);

        String whereTurma = DataBaseContract.Turma._ID+"="+aulaNova.getId();
        valorTurma.put(DataBaseContract.Turma.COLUNA_TURMA,aulaNova.getTurma());
        bd.update(DataBaseContract.Turma.NOME_TABELA,valorTurma,whereTurma,null);

        String whereCurso = DataBaseContract.Curso._ID+"="+aulaNova.getId();
        valorCurso.put(DataBaseContract.Curso.COLUNA_CURSO,aulaNova.getCurso());
        bd.update(DataBaseContract.Curso.NOME_TABELA,valorCurso,whereCurso,null);

        String whereProfessor = DataBaseContract.Professor._ID+"="+aulaNova.getId();
        valorProfessor.put(DataBaseContract.Professor.COLUNA_PROFESSOR,aulaNova.getProfessor());
        bd.update(DataBaseContract.Professor.NOME_TABELA,valorProfessor,whereProfessor,null);

        String whereDia = DataBaseContract.Dia._ID+"="+aulaNova.getId();
        valorDia.put(DataBaseContract.Dia.COLUNA_DIA,aulaNova.getDiaSemana());
        bd.update(DataBaseContract.Dia.NOME_TABELA,valorDia,whereDia,null);

        String whereHora = DataBaseContract.Horario._ID+"="+aulaNova.getId();
        valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_INICIO,aulaNova.getHora());
        valorHora.put(DataBaseContract.Horario.COLUNA_HORARIO_FINAL,aulaNova.getHoraTermino());
        bd.update(DataBaseContract.Horario.NOME_TABELA,valorHora,whereHora,null);

        String whereSala = DataBaseContract.Sala._ID+"="+aulaNova.getId();
        valorSala.put(DataBaseContract.Sala.COLUNA_SALA,aulaNova.getSala());
        bd.update(DataBaseContract.Sala.NOME_TABELA,valorSala,whereSala,null);

        bd.close();

    }


}


