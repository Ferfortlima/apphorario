package com.example.marce.materialdesign.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.controller.Aviso;
import com.example.marce.materialdesign.controller.EdicaoActivity;
import com.example.marce.materialdesign.controller.MainActivity;
import com.example.marce.materialdesign.model.Aula;


public class DetalhesDisciplinaFragment extends DialogFragment {

    Aula aula;
    TextView nome;
    TextView professor;
    TextView horario;
    TextView horarioTermino;
    TextView turma;
    TextView sala;
    TextView curso;
    TextView semestre;
    TextView diaSemana;

    Aviso aviso = new Aviso(this.getActivity());
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;

        getDialog().setCanceledOnTouchOutside(true);


        //remover divisor azul do dialog
        Bundle args = getArguments();
        View rootView = inflater.inflate(R.layout.fragment_detalhes_disciplina, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        toolbar.inflateMenu(R.menu.menu_dialog_fragment);

        Bundle bundle = getArguments();

        aula = (Aula) bundle.getSerializable("aula");

        nome = (TextView) rootView.findViewById(R.id.nome);
        professor = (TextView) rootView.findViewById(R.id.professor);
        horario = (TextView) rootView.findViewById(R.id.horario);
        turma = (TextView) rootView.findViewById(R.id.turma);
        sala = (TextView) rootView.findViewById(R.id.sala);
        curso = (TextView) rootView.findViewById(R.id.curso);
        semestre = (TextView) rootView.findViewById(R.id.semestre);
        diaSemana = (TextView) rootView.findViewById(R.id.diaSemana);
        horarioTermino = (TextView) rootView.findViewById(R.id.horario_termino);

        professor.setText(getString(R.string.professor) + ": " + aula.getProfessor());
        horario.setText(getString(R.string.horarioInicio) + ": " + aula.getHora());
        horarioTermino.setText(getString(R.string.horarioTermino) + ": " + aula.getHoraTermino());
        turma.setText(getString(R.string.turma) + ": " + aula.getTurma());
        sala.setText(getString(R.string.sala) + ": " + aula.getSala());
        curso.setText(getString(R.string.curso) + ": " + aula.getCurso());
        semestre.setText(getString(R.string.semestre) + ": " + aula.getSemestre());
        diaSemana.setText(getString(R.string.diaDaSemana) + ": " + aula.getDiaSemana());

        nome.setText(aula.getNome());

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.action_editar:

                        Intent intent = new Intent(getActivity(), EdicaoActivity.class);

                        Bundle bundle = new Bundle();

                        bundle.putSerializable("aula",aula);

                        intent.putExtra("aula",aula);

                        getActivity().startActivity(intent);

                        getDialog().dismiss();

                        return true;

                    case R.id.action_excluir:

                        MainActivity mainActivity = (MainActivity) getActivity();


                        aviso.showDialogExclusao(mainActivity,getString(R.string.pergunta_exclusao), aula, getDialog());


                        return true;
                    default:

                        return true;

                }
            }
        });
        return rootView;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }




}
