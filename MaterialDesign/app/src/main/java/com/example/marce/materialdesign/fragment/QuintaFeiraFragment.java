package com.example.marce.materialdesign.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.controller.ListAdapterAula;
import com.example.marce.materialdesign.controller.MainActivity;
import com.example.marce.materialdesign.model.Aula;

import java.util.ArrayList;
import java.util.List;

public class QuintaFeiraFragment extends Fragment {

    private TextView emptyView;
    private TextView textView;
    private ListView listView;
    private List disciplinas = new ArrayList<Aula>();
    private List discAux ;
    ListAdapterAula list;
    View rootView;

    List <Aula> aux;

    public QuintaFeiraFragment() {

    }

    @Override
    public void onStart() {
        super.onStart();

        MainActivity activity = (MainActivity) getActivity();
        aux=new ArrayList();
        discAux =new ArrayList();


        aux.clear();
        discAux.clear();
        disciplinas.clear();

        aux = activity.getListaAulasQuintaFeira();

        if(aux.size() > 0) {
            for (int i = 0; i < aux.size(); i++) {
                disciplinas.add(aux.get(i));
            }
        }


        if(aux.size() > 0) {
            for (int i = 0; i < aux.size(); i++) {
                discAux.add(aux.get(i));
            }
        }

        refreshDisciplinas(discAux);

        listView.setAdapter(list);

        if (disciplinas.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            emptyView.setVisibility(View.GONE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        rootView = inflater.inflate(R.layout.fragment_quinta_feira, container, false);

        emptyView = (TextView) rootView.findViewById(R.id.empty_view);

        textView = (TextView) rootView.findViewById(R.id.disc);
        listView = (ListView) rootView.findViewById(R.id.list);
        emptyView = (TextView) rootView.findViewById(R.id.empty_view);


        emptyView = (TextView) rootView.findViewById(R.id.empty_view);




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                goToAttract(arg1);
            }

        });



        list = new ListAdapterAula(disciplinas, rootView.getContext());


        return rootView;
    }

    public void goToAttract(View v)
    {
        Intent intent = new Intent(getActivity(), DetalhesDisciplinaFragment.class);
        startActivity(intent);
    }

    /**
     * Método que atualiza as listas do listView
     * @param events
     */
    public void refreshDisciplinas(List<Aula> events){
        disciplinas.clear();
        disciplinas.addAll(events);
        list = new ListAdapterAula(disciplinas, rootView.getContext());
        list.notifyDataSetChanged();
    }}