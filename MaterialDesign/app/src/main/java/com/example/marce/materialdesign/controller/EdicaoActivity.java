package com.example.marce.materialdesign.controller;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.dao.DataBaseDAO;
import com.example.marce.materialdesign.model.Aula;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class EdicaoActivity extends AppCompatActivity {

    private Aula aula;
    private List<Aula> aulasDisciplina = new ArrayList<Aula>();
    private List<Aula> diaSalaHora = new ArrayList<Aula>();
    // variáveis que representam os atributos da disciplinas, ou seja, todas as aulas têm em comum
    private EditText nome;
    private EditText professor;
    private EditText curso;
    private EditText turma;
    private EditText semestre;
    // variáveis que representam se os checkbox's estão selecionados
    private boolean booleanSegunda;
    private boolean booleanTerca;
    private boolean booleanQuarta;
    private boolean booleanQuinta;
    private boolean booleanSexta;
    private boolean booleanSabado;

    //variavel da notificacao
    private CheckBox tempoNotificacao;
    private boolean notificar;

    // variáveis que representam os dias da semana que a disciplinas contém aula
    private boolean isAulaSegundaFeira = false;
    private boolean isAulaTercaFeira = false;
    private boolean isAulaQuartaFeira = false;
    private boolean isAulaQuintaFeira = false;
    private boolean isAulaSextaFeira = false;
    private boolean isAulaSabado = false;

    // variáveis que representam os checkbox's da activity
    private CheckBox segunda;
    private CheckBox terca;
    private CheckBox quarta;
    private CheckBox quinta;
    private CheckBox sexta;
    private CheckBox sabado;

    // variáveis que os campos sala, horario de início e horário de término de cada dia da semana
    private EditText salaSegunda;
    private TextView horaInicioSegunda;
    private TextView horaFimSegunda;
    private EditText salaTerca;
    private TextView horaInicioTerca;
    private TextView horaFimTerca;
    private EditText salaQuarta;
    private TextView horaInicioQuarta;
    private TextView horaFimQuarta;
    private EditText salaQuinta;
    private TextView horaInicioQuinta;
    private TextView horaFimQuinta;
    private EditText salaSexta;
    private TextView horaInicioSexta;
    private TextView horaFimSexta;
    private EditText salaSabado;
    private TextView horaInicioSabado;
    private TextView horaFimSabado;
    // variáveis que representam o valor inicial de cada campo ao iniciar a activity
    private String salaSegundaOpen;
    private String horaInicioSegundaOpen;
    private String horaFimSegundaOpen;
    private String salaTercaOpen;
    private String horaInicioTercaOpen;
    private String horaFimTercaOpen;
    private String salaQuartaOpen;
    private String horaInicioQuartaOpen;
    private String horaFimQuartaOpen;
    private String salaQuintaOpen;
    private String horaInicioQuintaOpen;
    private String horaFimQuintaOpen;
    private String salaSextaOpen;
    private String horaInicioSextaOpen;
    private String horaFimSextaOpen;
    private String salaSabadoOpen;
    private String horaInicioSabadoOpen;
    private String horaFimSabadoOpen;

    private Toolbar myToolbar;

    Aviso aviso = new Aviso(EdicaoActivity.this);

    DataBaseDAO dao = new DataBaseDAO(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edicao);
        // iniciar a activity com o teclado escondido
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED);
        // pegar dados da disciplina selecionada
        Bundle b = getIntent().getExtras();
        aula = (Aula) b.getSerializable("aula");
        // iniciar váriaveis
        nome = (EditText) findViewById(R.id.textInput_nome);
        professor = (EditText) findViewById(R.id.textInput_professor);
        curso = (EditText) findViewById(R.id.textInput_curso);
        turma = (EditText) findViewById(R.id.textInput_turma);
        semestre = (EditText) findViewById(R.id.textInput_semestre);

        segunda = (CheckBox) findViewById(R.id.checkBox_segunda);
        terca = (CheckBox) findViewById(R.id.checkBox_terca);
        quarta = (CheckBox) findViewById(R.id.checkBox_quarta);
        quinta = (CheckBox) findViewById(R.id.checkBox_quinta);
        sexta = (CheckBox) findViewById(R.id.checkBox_sexta);
        sabado = (CheckBox) findViewById(R.id.checkBox_sabado);
        //tempoNotificacao = (CheckBox) findViewById(R.id.checkBox_tempoNotificacaoEdicao);


        nome.setText(aula.getNome());
        professor.setText(aula.getProfessor());
        curso.setText(aula.getCurso());
        turma.setText(aula.getTurma());
        semestre.setText(aula.getSemestre());

        salaSegunda = (EditText) findViewById(R.id.sala_segunda);
        horaInicioSegunda = (TextView) findViewById(R.id.hora_inicio_segunda);
        horaFimSegunda = (TextView) findViewById(R.id.hora_fim_segunda);
        salaTerca = (EditText) findViewById(R.id.sala_terca);
        horaInicioTerca = (TextView) findViewById(R.id.hora_inicio_terca);
        horaFimTerca = (TextView) findViewById(R.id.hora_fim_terca);
        salaQuarta = (EditText) findViewById(R.id.sala_quarta);
        horaInicioQuarta = (TextView) findViewById(R.id.hora_inicio_quarta);
        horaFimQuarta = (TextView) findViewById(R.id.hora_fim_quarta);
        salaQuinta = (EditText) findViewById(R.id.sala_quinta);
        horaInicioQuinta = (TextView) findViewById(R.id.hora_inicio_quinta);
        horaFimQuinta = (TextView) findViewById(R.id.hora_fim_quinta);
        salaSexta = (EditText) findViewById(R.id.sala_sexta);
        horaInicioSexta = (TextView) findViewById(R.id.hora_inicio_sexta);
        horaFimSexta = (TextView) findViewById(R.id.hora_fim_sexta);
        salaSabado = (EditText) findViewById(R.id.sala_sabado);
        horaInicioSabado = (TextView) findViewById(R.id.hora_inicio_sabado);
        horaFimSabado = (TextView) findViewById(R.id.hora_fim_sabado);

        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descobrirAulasDisciplina();

        exibirInformacoesDiasDaSemana();

        preencherInformacoesDiasDaSemana();

        salvarStatusInicial();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edicao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (verificarAlteracoes(aula) || verificarAlteracoesAulaSegundaFeira(salaSegunda.getText().toString(), horaInicioSegunda.getText().toString(), horaFimSegunda.getText().toString())
                        || verificarAlteracoesAulaTercaFeira(salaTerca.getText().toString(), horaInicioTerca.getText().toString(), horaFimTerca.getText().toString())
                        || verificarAlteracoesAulaQuartaFeira(salaQuarta.getText().toString(), horaInicioQuarta.getText().toString(), horaFimQuarta.getText().toString())
                        || verificarAlteracoesAulaQuintaFeira(salaQuinta.getText().toString(), horaInicioQuinta.getText().toString(), horaFimQuinta.getText().toString())
                        || verificarAlteracoesAulaSextaFeira(salaSexta.getText().toString(), horaInicioSexta.getText().toString(), horaFimSexta.getText().toString())
                        || verificarAlteracoesAulaSabado(salaSabado.getText().toString(), horaInicioSabado.getText().toString(), horaFimSabado.getText().toString())){
                    aviso.showDialog(getString(R.string.descarte_alteracao));

            }

                 else {
                    finish();
                }
                return true;

            case R.id.action_salvar:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.realizar_alteracao)).setTitle(null)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                if (isCamposPreenchidos() && verificarCampos() ) {
                    if (countChecked(booleanSegunda, booleanTerca, booleanQuarta, booleanQuinta, booleanSexta, booleanSabado) > 0) {

                        //verificarNotificacao();

                        verificarAulasDeletadas();

                        getDiaSalaHora(booleanSegunda, booleanTerca, booleanQuarta, booleanQuinta, booleanSexta, booleanSabado);

                        salvarAlteracoes();

                        verificarAulasAdicionadas();

                        finish();

                        aviso.toastMessager(getString(R.string.alteracao_disciplina_sucesso));

                        }else{
                            aviso.toastMessager(getString(R.string.minimo_um_checkbox));
                        }

                    }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (verificarAlteracoes(aula) || verificarAlteracoesAulaSegundaFeira(salaSegunda.getText().toString(), horaInicioSegunda.getText().toString(), horaFimSegunda.getText().toString())
                || verificarAlteracoesAulaTercaFeira(salaTerca.getText().toString(), horaInicioTerca.getText().toString(), horaFimTerca.getText().toString())
                || verificarAlteracoesAulaQuartaFeira(salaQuarta.getText().toString(), horaInicioQuarta.getText().toString(), horaFimQuarta.getText().toString())
                || verificarAlteracoesAulaQuintaFeira(salaQuinta.getText().toString(), horaInicioQuinta.getText().toString(), horaFimQuinta.getText().toString())
                || verificarAlteracoesAulaSextaFeira(salaSexta.getText().toString(), horaInicioSexta.getText().toString(), horaFimSexta.getText().toString())
                || verificarAlteracoesAulaSabado(salaSabado.getText().toString(), horaInicioSabado.getText().toString(), horaFimSabado.getText().toString())) {
            aviso.showDialog(getString(R.string.descarte_alteracao));
        } else {
            finish();
        }
    }


//    private void criaNotificacao(Notification notification, int delay) {
//
//        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
//        notificationIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, 1);
//        notificationIntent.putExtra(AlarmReceiver.NOTIFICATION, notification);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        long futureInMillis = SystemClock.elapsedRealtime() + delay;
//        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
//    }
//
//    private Notification getNotification(String content) {
//        Notification.Builder builder = new Notification.Builder(this);
//        builder.setContentTitle("");
//        builder.setContentText(content);
//        builder.setSmallIcon(R.mipmap.ic_launcher_relogio);
//        return builder.build();
//    }
//
//    public void verificarNotificacao() {
//
//        if (tempoNotificacao.isChecked()) {
//            notificar = true;
//        }else{
//            notificar = false;
//        }
//
//        if(notificar == true){
//            criaNotificacao(getNotification("batata"),10);
//        }
//    }


    /**
     * Método que salva as alterações do usuário e atualiza as informações no banco de dados
     */

    public void salvarAlteracoes(){
        int tempoNotificacao;
        if(notificar){
            tempoNotificacao = 60;
        }else{
            tempoNotificacao = 0;
        }

        for (int i = 0; i < aulasDisciplina.size(); i++) {
            String hora;

            aulasDisciplina.get(i).setNome(nome.getText().toString());
            aulasDisciplina.get(i).setProfessor(professor.getText().toString());
            aulasDisciplina.get(i).setCurso(curso.getText().toString());
            aulasDisciplina.get(i).setTurma(turma.getText().toString());
            aulasDisciplina.get(i).setSemestre(semestre.getText().toString());
            aulasDisciplina.get(i).setTempoNotificacao(tempoNotificacao);

            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.segunda))) {

                aulasDisciplina.get(i).setSala(salaSegunda.getText().toString());
                hora = horaInicioSegunda.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimSegunda.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.terca))) {

                aulasDisciplina.get(i).setSala(salaTerca.getText().toString());
                hora = horaInicioTerca.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimTerca.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quarta))) {

                aulasDisciplina.get(i).setSala(salaQuarta.getText().toString());
                hora = horaInicioQuarta.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimQuarta.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quinta))) {

                aulasDisciplina.get(i).setSala(salaQuinta.getText().toString());
                hora = horaInicioQuinta.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimQuinta.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sexta))) {

                aulasDisciplina.get(i).setSala(salaSexta.getText().toString());
                hora = horaInicioSexta.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimSexta.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sabado))) {

                aulasDisciplina.get(i).setSala(salaSabado.getText().toString());
                hora = horaInicioSabado.getText().toString();
                aulasDisciplina.get(i).setHora(hora.substring(hora.length()-5, hora.length()));
                hora = horaFimSabado.getText().toString();
                aulasDisciplina.get(i).setHoraTermino(hora.substring(hora.length()-5, hora.length()));

            }

            dao.update(aulasDisciplina.get(i));

        }

    }

    /**
     * Método que verifica se alguma aula foi deletada ao salvar as alterações
     */
    public void verificarAulasDeletadas() {

        for(int i = 0; i < aulasDisciplina.size(); i++){
            if(aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.segunda))){
                if(booleanSegunda == false){
                    dao.delete(aulasDisciplina.get(i));
                }
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.terca))) {
                if (booleanTerca == false) {
                    dao.delete(aulasDisciplina.get(i));
                }
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quarta))) {
                if (booleanQuarta == false) {
                    dao.delete(aulasDisciplina.get(i));
                }
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quinta))) {
                if (booleanQuinta == false) {
                    dao.delete(aulasDisciplina.get(i));
                }
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sexta))) {
                if (booleanSexta == false) {
                    dao.delete(aulasDisciplina.get(i));
                }
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sabado))) {
                if (booleanSabado == false) {
                    dao.delete(aulasDisciplina.get(i));
                }
            }
        }
    }

    /**
     * Método que verifica se o usuário selecionou um horário válido
     * @param horaInicio
     * @param horaFinal
     * @param tv
     * @return
     */
    public boolean verificarHora(String horaInicio, String horaFinal, TextView tv) {
        boolean tudook = true;

        //verifica se a hora de término é maior que a de inicio
        List<String> listhoras = new ArrayList<>();
        listhoras.add(horaInicio);
        listhoras.add(horaFinal);
        Collections.sort(listhoras);
        if(listhoras.get(0).equals(horaFinal)){
            tv.setTextColor(Color.RED);
            tv.requestFocus();
            tudook = false;
        }

        if(tudook){
            tv.setTextColor(Color.BLACK);
            tv.clearFocus();
        }
        return tudook;
    }

    /**
     * Método que verifica se o usuário preencheu o campo sala com uma informação válida
     * @param sala
     * @return
     */
    public boolean verificaSala(TextInputLayout sala) {
        boolean tudook = true;
        if (sala.getEditText().getText().toString().trim().length() == 0) {
            sala.setErrorEnabled(true);
            sala.setError(getString(R.string.campo_vazio));
            tudook = false;
            sala.requestFocus();
        }else {
            sala.setError(null);
            sala.clearFocus();
        }
        return tudook;
    }

    /**
     * Método que verifica se todos os campos da disciplina foram preenchidos corretamente
     * @return
     */
    public boolean verificarCampos() {
            if (verificarHorasSalas()) {
                return true;
            } else{
                return false;
            }
    }

     /*
      * Método para setar os erros nos campos de salas e horas que estiverem incorretos
      * @return boolean retorna TRUE se tudo estiver preenchido corretamente, caso contrário FALSE
      */
    public boolean verificarHorasSalas() {
        boolean tudook = true;
        boolean avisoHora = false;
        TextView tvInicio;
        TextView tvFim;
        String horaInicio;
        String horaFim;

        if (booleanSegunda) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaseg);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_segunda);
            tvFim = (TextView) findViewById(R.id.hora_fim_segunda);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanTerca) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salater);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_terca);
            tvFim = (TextView) findViewById(R.id.hora_fim_terca);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanQuarta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaqua);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quarta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quarta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanQuinta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaqui);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quinta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quinta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanSexta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salasex);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sexta);
            tvFim = (TextView) findViewById(R.id.hora_fim_sexta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanSabado) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salasab);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sabado);
            tvFim = (TextView) findViewById(R.id.hora_fim_sabado);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//            }else
            if(!verificarHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if(avisoHora) {
            aviso.ShowDialogInformacao(getString(R.string.alterar_horario_inicio));
        }
        return tudook;
    }

    /**
     * Método que verifica se alguma aula foi adiciona à disciplina ao salvar suas alterações
     */
    public void verificarAulasAdicionadas(){
        int tempoNotificacao;
        if(notificar){
            tempoNotificacao = 60;
        }else{
            tempoNotificacao = 0;
        }
        if(isAulaSegundaFeira == false && booleanSegunda == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioSegunda.getText().toString().substring(horaInicioSegunda.length()-5, horaInicioSegunda.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaSegunda.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Segunda-Feira");
            aula.setHoraTermino(horaFimSegunda.getText().toString().substring(horaFimSegunda.length()-5, horaFimSegunda.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }


        if(isAulaTercaFeira == false && booleanTerca == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioTerca.getText().toString().substring(horaInicioTerca.length()-5, horaInicioTerca.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaTerca.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Terça-Feira");
            aula.setHoraTermino(horaFimTerca.getText().toString().substring(horaFimTerca.length()-5, horaFimTerca.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }


        if(isAulaQuartaFeira == false && booleanQuarta == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioQuarta.getText().toString().substring(horaInicioQuarta.length()-5, horaInicioQuarta.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaQuarta.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Quarta-Feira");
            aula.setHoraTermino(horaFimQuarta.getText().toString().substring(horaFimQuarta.length()-5, horaFimQuarta.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }


        if(isAulaQuintaFeira == false && booleanQuinta == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioQuinta.getText().toString().substring(horaInicioQuinta.length()-5, horaInicioQuinta.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaQuinta.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Quinta-Feira");
            aula.setHoraTermino(horaFimQuinta.getText().toString().substring(horaFimQuinta.length()-5, horaFimQuinta.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }

        if(isAulaSextaFeira == false && booleanSexta == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioSexta.getText().toString().substring(horaInicioSexta.length()-5, horaInicioSexta.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaSexta.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Sexta-Feira");
            aula.setHoraTermino(horaFimSexta.getText().toString().substring(horaFimSexta.length()-5, horaFimSexta.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }


        if(isAulaSabado == false && booleanSabado == true){

            Aula aula = new Aula();
            aula.setNome(nome.getText().toString());
            aula.setProfessor(professor.getText().toString());
            aula.setHora(horaInicioSabado.getText().toString().substring(horaInicioSabado.length()-5, horaInicioSabado.length()));
            aula.setTurma(turma.getText().toString());
            aula.setSala(salaSabado.getText().toString());
            aula.setCurso(curso.getText().toString());
            aula.setSemestre(semestre.getText().toString());
            aula.setDiaSemana("Sábado");
            aula.setHoraTermino(horaFimSabado.getText().toString().substring(horaFimSabado.length()-5, horaFimSabado.length()));
            aula.setTempoNotificacao(tempoNotificacao);

            dao.insert(aula);
        }



    }

    /**
     * Método para inflar o timepickerdialog correspondente ao dia e horario(inicial ou final) selecionado
     *
     * @param v view que contem as tags
     */
    public void ShowTimepicker(View v) {
        String dia = (String) v.getTag();
        DialogFragment newFragment;
        switch (dia) {
            case "segundainicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_segunda", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "tercainicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_terca", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "quartainicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_quarta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "quintainicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_quinta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "sextainicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_sexta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "sabadoinicio":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_inicio_sabado", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;

            case "segundafim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_segunda", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "tercafim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_terca", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "quartafim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_quarta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "quintafim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_quinta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "sextafim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_sexta", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;
            case "sabadofim":
                newFragment = new EdicaoActivity.TimePickerFragment("hora_fim_sabado", getPackageName());
                newFragment.show(getFragmentManager(), "TimePicker");
                break;

            default:
                break;
        }

    }

    /**
     * Classe para gerenciar a ação do timepicker
     */
    @SuppressLint("ValidFragment")
    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        private String id;
        private String packageName;

        public TimePickerFragment(){

        }
        public TimePickerFragment(String id, String packageName) {
            this.id = id;
            this.packageName = packageName;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            //Use the current time as the default values for the time picker
            final Calendar c = Calendar.getInstance();
            int hora = c.get(Calendar.HOUR_OF_DAY);
            int minuto = c.get(Calendar.MINUTE);

            //Create and return a new instance of TimePickerDialog
            return new TimePickerDialog(getActivity(),this, hora, minuto, DateFormat.is24HourFormat(getActivity()));
        }

        //onTimeSet() callback method
        public void onTimeSet(TimePicker view, int hora, int minuto){
            String horaAux = new String();

            //busca o textview a ser editado com o horário informado
            int resID = getResources().getIdentifier(id, "id", packageName);
            TextView tv = (TextView) getActivity().findViewById(resID);
            //Altera o valor do horário de inicio com o valor informado
            horaAux = tv.getText().toString();
            horaAux = horaAux.substring(0,horaAux.length()-5);
            tv.setText(horaAux + String.format("%02d:%02d", hora, minuto));
        }
    }

    /*
     * Método para verificar se o usuário realizou alterações na disciplina antes de cancelar a operação
     */

    public boolean verificarAlteracoes(Aula aula) {

        boolean retorno;
        if (aula.getNome().equals(nome.getText().toString()) && aula.getProfessor().equals(professor.getText().toString())
                && aula.getCurso().equals(curso.getText().toString()) && aula.getSemestre().equals(semestre.getText().toString())
                && aula.getTurma().equals(turma.getText().toString())) {
            retorno = false;
        } else {
            retorno = true;
        }

        return retorno;
    }

    /*
     * Método para verificar se todos os campos foram preenchidos, caso contrario seta uma mensagem
     * de erro no campo a ser preenchido
     */
    public boolean isCamposPreenchidos() {


        TextInputLayout inputNome = (TextInputLayout) findViewById(R.id.textInputLayout_nome);
//        TextInputLayout inputProfessor = (TextInputLayout) findViewById(R.id.textInputLayout_profesor);
//        TextInputLayout inputCurso = (TextInputLayout) findViewById(R.id.textInputLayout_curso);
//        TextInputLayout inputTurma = (TextInputLayout) findViewById(R.id.textInputLayout_turma);
//        TextInputLayout inputSemestre = (TextInputLayout) findViewById(R.id.textInputLayout_semestre);


        boolean tudook = true;
        boolean isNomeOk = false;
//        boolean isProfessorOk = false;
//        boolean isCursoOk = false;
//        boolean isTurmaOk = false;
//        boolean isSemestreOk = false;


        if (inputNome.getEditText().getText().toString().trim().length() == 0) {
            inputNome.setError(getString(R.string.campo_vazio));
            inputNome.setErrorEnabled(true);
            tudook = false;
        } else {
            isNomeOk = true;
            nome.setError(null);
        }

//        if (inputProfessor.getEditText().getText().toString().trim().length() == 0) {
//            inputProfessor.setError(getString(R.string.campo_vazio));
//            inputProfessor.setErrorEnabled(true);
//            tudook = false;
//        } else {
//            isProfessorOk = true;
//            professor.setError(null);
//        }
//        if (inputCurso.getEditText().getText().toString().trim().length() == 0) {
//            inputCurso.setError(getString(R.string.campo_vazio));
//            inputCurso.setErrorEnabled(true);
//            tudook = false;
//        } else {
//            isCursoOk = true;
//            curso.setError(null);
//        }
//
//        if (inputSemestre.getEditText().getText().toString().trim().length() == 0) {
//            inputSemestre.setError(getString(R.string.campo_vazio));
//            inputSemestre.setErrorEnabled(true);
//            tudook = false;
//        } else {
//            isSemestreOk = true;
//            semestre.setError(null);
//        }
//
//        if (inputTurma.getEditText().getText().toString().trim().length() == 0) {
//            inputTurma.setError(getString(R.string.campo_vazio));
//            inputTurma.setErrorEnabled(true);
//            tudook = false;
//        } else {
//            isTurmaOk = true;
//            turma.setError(null);
//        }
        if (!isNomeOk) {
            nome.requestFocus();
        }
//        if (!isTurmaOk) {
//            turma.requestFocus();
//        }
//        if (!isSemestreOk) {
//            semestre.requestFocus();
//        }
//        if (!isCursoOk) {
//            curso.requestFocus();
//        }
//        if (!isProfessorOk) {
//            professor.requestFocus();
//        }

        return tudook;
    }

    /**
     * Método que preenche os campos da disciplina e das aulas ao iniciar a activity
     */
    public void preencherInformacoesDiasDaSemana() {
        String inicio;
        String fim;

        for (int i = 0; i < aulasDisciplina.size(); i++) {
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.segunda))) {

                salaSegunda.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioSegunda.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaFimSegunda.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioSegunda.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimSegunda.setText(fim +aulasDisciplina.get(i).getHoraTermino());

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.terca))) {

                salaTerca.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioTerca.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaFimTerca.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioTerca.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimTerca.setText(fim + aulasDisciplina.get(i).getHoraTermino());

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quarta))) {

                salaQuarta.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioQuarta.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaFimQuarta.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioQuarta.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimQuarta.setText(fim + aulasDisciplina.get(i).getHoraTermino());

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quinta))) {

                salaQuinta.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioQuinta.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaInicioQuinta.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioQuinta.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimQuinta.setText(fim + aulasDisciplina.get(i).getHoraTermino());

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sexta))) {

                salaSexta.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioSexta.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaInicioSexta.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioSexta.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimSexta.setText(fim + aulasDisciplina.get(i).getHoraTermino());

            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sabado))) {

                salaSabado.setText(aulasDisciplina.get(i).getSala());

                inicio = horaInicioSabado.getText().toString();
                inicio = inicio.substring(0, inicio.length()-5);
                fim = horaInicioSabado.getText().toString();
                fim = fim.substring(0, fim.length()-5);

                horaInicioSabado.setText(inicio + aulasDisciplina.get(i).getHora());
                horaFimSabado.setText(fim + aulasDisciplina.get(i).getHoraTermino());

            }
        }
    }

    /**
     * Método que descobre todas as aulas relacionadas a auma disciplina
     */
    public void descobrirAulasDisciplina() {

        List<Aula> todasAulasBanco = dao.select();

        for (int i = 0; i < todasAulasBanco.size(); i++) {
            if (todasAulasBanco.get(i).getNome().equals(aula.getNome())) {
                aulasDisciplina.add(todasAulasBanco.get(i));
            }
        }

        for(int i = 0; i < aulasDisciplina.size(); i++){

                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.segunda))) {
                    isAulaSegundaFeira = true;
                }
                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.terca))) {
                    isAulaTercaFeira = true;

                }
                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quarta))) {
                    isAulaQuartaFeira = true;
                }
                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quinta))) {
                    isAulaQuintaFeira = true;
                }
                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sexta))) {
                    isAulaSextaFeira = true;

                }
                if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sabado))) {
                    isAulaSabado = true;
                }
            }
    }

    /**
     * Método que exibe os dias da semana que a disciplina têm aulas
     */
    public void exibirInformacoesDiasDaSemana() {

        for (int i = 0; i < aulasDisciplina.size(); i++) {
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.segunda))) {
                segunda.setChecked(true);
                LinearLayout AulaSegunda = (LinearLayout) findViewById(R.id.layout_aula_segunda);
                AulaSegunda.setVisibility(View.VISIBLE);
                booleanSegunda = true;
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.terca))) {
                terca.setChecked(true);
                LinearLayout AulaTerca = (LinearLayout) findViewById(R.id.layout_aula_terca);
                AulaTerca.setVisibility(View.VISIBLE);
                booleanTerca = true;
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quarta))) {
                quarta.setChecked(true);
                LinearLayout AulaQuarta = (LinearLayout) findViewById(R.id.layout_aula_quarta);
                AulaQuarta.setVisibility(View.VISIBLE);
                booleanQuarta = true;
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.quinta))) {
                quinta.setChecked(true);
                LinearLayout AulaQuinta = (LinearLayout) findViewById(R.id.layout_aula_quinta);
                AulaQuinta.setVisibility(View.VISIBLE);
                booleanQuinta = true;
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sexta))) {
                sexta.setChecked(true);
                LinearLayout AulaSexta = (LinearLayout) findViewById(R.id.layout_aula_sexta);
                AulaSexta.setVisibility(View.VISIBLE);
                booleanSexta = true;
            }
            if (aulasDisciplina.get(i).getDiaSemana().equals(getString(R.string.sabado))) {
                sabado.setChecked(true);
                LinearLayout AulaSabado = (LinearLayout) findViewById(R.id.layout_aula_sabado);
                AulaSabado.setVisibility(View.VISIBLE);
                booleanSabado = true;
            }
            /*
            if(aulasDisciplina.get(i).getTempoNotificacao() != 0){
                tempoNotificacao.setChecked(true);
            }
            */
        }

    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Segunda-Feira ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaSegundaFeira(String sala, String horaInicio, String horaFim){
      if(isAulaSegundaFeira || booleanSegunda == true){
          if(sala.equals(salaSegundaOpen) && horaInicio.equals(horaInicioSegundaOpen) && horaFim.equals(horaFimSegundaOpen.toString())){

              return false;

          }else{


              return true;
          }
      }else{
          return false;
      }

    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Terça-Feira ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaTercaFeira(String sala, String horaInicio, String horaFim){
        if(isAulaTercaFeira || booleanTerca == true){
            if(sala.equals(salaTercaOpen) && horaInicio.equals(horaInicioTercaOpen) && horaFim.equals(horaFimTercaOpen)){

                return false;

            }else{

                return true;
            }
        }
       else{
            return false;
        }
    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Quarta-Feira ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaQuartaFeira(String sala, String horaInicio, String horaFim){
        if(isAulaQuartaFeira || booleanQuarta==true){
            if(sala.equals(salaQuartaOpen) && horaInicio.equals(horaInicioQuartaOpen) && horaFim.equals(horaFimQuartaOpen)){

                return false;

            }else{

                return true;
            }
        }else{
            return false;
        }

    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Quinta-Feira ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaQuintaFeira(String sala, String horaInicio, String horaFim){
        if(isAulaQuintaFeira || (booleanQuinta == true)){
            if(sala.equals(salaQuintaOpen) && horaInicio.equals(horaInicioQuintaOpen) && horaFim.equals(horaFimQuintaOpen)){

                return false;

            }else{

                return true;
            }
        }else{
            return false;
        }

    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Sexta-Feira ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaSextaFeira(String sala, String horaInicio, String horaFim){
        if(isAulaSextaFeira || (booleanSexta ==true)){
            if(sala.equals(salaSextaOpen) && horaInicio.equals(horaInicioSextaOpen) && horaFim.equals(horaFimSextaOpen)){

                return false;

            }else{

                return true;
            }
        }else{
            System.out.println("sala Sexta" + sala);
            return false;
        }

    }

    /**
     * Método que verifica se o usuário realizou alterações no dia de Sábado ao sair da activity de edição
     * @param sala
     * @param horaInicio
     * @param horaFim
     * @return
     */
    public boolean verificarAlteracoesAulaSabado(String sala, String horaInicio, String horaFim){
        if(isAulaSabado || (booleanSabado == true )){
            if(sala.equals(salaSabadoOpen) && horaInicio.equals(horaInicioSabadoOpen) && horaFim.equals(horaFimSabadoOpen)){

                return false;

            }else{

                return true;
            }
        }else{
            return false;
        }

    }

    /**
     * Método que salva os valores originais da disciplina e aulas ao iniciar a activity edição
     */
    public void salvarStatusInicial(){
        if(isAulaSegundaFeira){
            salaSegundaOpen = salaSegunda.getText().toString();
            horaInicioSegundaOpen = horaInicioSegunda.getText().toString();
            horaFimSegundaOpen = horaFimSegunda.getText().toString();
        }
        if(isAulaTercaFeira){
            salaTercaOpen = salaTerca.getText().toString();
            horaInicioTercaOpen = horaInicioTerca.getText().toString();
            horaFimTercaOpen = horaFimTerca.getText().toString();
        }
        if(isAulaQuartaFeira){
            salaQuartaOpen = salaQuarta.getText().toString();
            horaInicioQuartaOpen = horaInicioQuarta.getText().toString();
            horaFimQuartaOpen = horaFimQuarta.getText().toString();
        }
        if(isAulaQuintaFeira){
            salaQuintaOpen = salaQuinta.getText().toString();
            horaInicioQuintaOpen = horaInicioQuinta.getText().toString();
            horaFimQuintaOpen = horaFimQuinta.getText().toString();
        }
        if(isAulaSextaFeira){
            salaSextaOpen = salaSexta.getText().toString();
            horaInicioSextaOpen = horaInicioSexta.getText().toString();
            horaFimSextaOpen = horaFimSexta.getText().toString();
        }
        if(isAulaSabado){
            salaSabadoOpen = salaSabado.getText().toString();
            horaInicioSabadoOpen = horaInicioSabado.getText().toString();
            horaFimSabadoOpen = horaFimSabado.getText().toString();
        }
    }

    /*
    * Método para monitorar os checkboxes e mudar a visibilidade dos layouts de acordo com o
    * que foi selecionado
    */
    public void onCheckboxClicked(View view) {
        TextInputLayout dia = (TextInputLayout) findViewById(R.id.textInputLayout_dias);
        boolean checked = ((CheckBox) view).isChecked();

        LinearLayout AulaSegunda = (LinearLayout) findViewById(R.id.layout_aula_segunda);
        LinearLayout AulaTerca = (LinearLayout) findViewById(R.id.layout_aula_terca);
        LinearLayout AulaQuarta = (LinearLayout) findViewById(R.id.layout_aula_quarta);
        LinearLayout AulaQuinta = (LinearLayout) findViewById(R.id.layout_aula_quinta);
        LinearLayout AulaSexta = (LinearLayout) findViewById(R.id.layout_aula_sexta);
        LinearLayout AulaSabado = (LinearLayout) findViewById(R.id.layout_aula_sabado);

        switch (view.getId()) {
            case R.id.checkBox_segunda:
                if (checked) {
                    booleanSegunda = true;
                    dia.setError(null);
                    AulaSegunda.setVisibility(View.VISIBLE);
                } else {
                    booleanSegunda = false;
                    AulaSegunda.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_terca:
                if (checked) {
                    booleanTerca = true;
                    dia.setError(null);
                    AulaTerca.setVisibility(View.VISIBLE);
                } else {
                    booleanTerca = false;
                    AulaTerca.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_quarta:
                if (checked) {
                    booleanQuarta = true;
                    dia.setError(null);
                    AulaQuarta.setVisibility(View.VISIBLE);
                } else {
                    booleanQuarta = false;
                    AulaQuarta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_quinta:
                if (checked) {
                    booleanQuinta = true;
                    dia.setError(null);
                    AulaQuinta.setVisibility(View.VISIBLE);
                } else {
                    booleanQuinta = false;
                    AulaQuinta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_sexta:
                if (checked) {
                    booleanSexta = true;
                    dia.setError(null);
                    AulaSexta.setVisibility(View.VISIBLE);
                } else {
                    booleanSexta = false;
                    AulaSexta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_sabado:
                if (checked) {
                    booleanSabado = true;
                    dia.setError(null);
                    AulaSabado.setVisibility(View.VISIBLE);
                } else {
                    booleanSabado = false;
                    AulaSabado.setVisibility(View.GONE);
                }
                break;
        }
    }

    /**
     * Método para verificar se o usuário adicionou ou removeu alguma aula
     * @param booleanSegunda
     * @param booleanTerca
     * @param booleanQuarta
     * @param booleanQuinta
     * @param booleanSexta
     * @param booleanSabado
     * @return
     */
    private int countChecked(boolean booleanSegunda, boolean booleanTerca, boolean booleanQuarta, boolean booleanQuinta, boolean booleanSexta, boolean booleanSabado) {
        int cont = 0;
        if(booleanSegunda){
            cont++;
        }
        if(booleanTerca){
            cont++;
        }
        if(booleanQuarta){
            cont++;
        }
        if(booleanQuinta){
            cont++;
        }
        if(booleanSexta){
            cont++;
        }
        if(booleanSabado){
            cont++;
        }
        return cont;
    }

    /*
    * Método para salvar os valores dos dias da semana da aula
    */
    public void getDiaSalaHora(boolean segunda, boolean terca, boolean quarta, boolean quinta, boolean sexta, boolean sabado) {
        diaSalaHora.clear();
        TextView tvInicio;
        TextView tvFim;
        Aula a;

        if (segunda) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_segunda);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_segunda);
            tvFim = (TextView) findViewById(R.id.hora_fim_segunda);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.segunda));

            diaSalaHora.add(a);
        }
        if (terca) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_terca);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_terca);
            tvFim = (TextView) findViewById(R.id.hora_fim_terca);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.terca));

            diaSalaHora.add(a);
        }
        if (quarta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_quarta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quarta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quarta);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.quarta));

            diaSalaHora.add(a);
        }
        if (quinta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_quinta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quinta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quinta);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.quinta));

            diaSalaHora.add(a);
        }
        if (sexta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_sexta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sexta);
            tvFim = (TextView) findViewById(R.id.hora_fim_sexta);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.sexta));

            diaSalaHora.add(a);
        }
        if (sabado) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_sabado);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sabado);
            tvFim = (TextView) findViewById(R.id.hora_fim_sabado);

            a = preencheAula(a,tvInicio, tvFim, sala, getString(R.string.sabado));

            diaSalaHora.add(a);
        }
    }

    /**
     * Método que insere os dados informados pelo usuário em um objeto aula
     * @param a  Objeto Aula a ser preenchido
     * @param tvInicio textview com a hora de inicio
     * @param tvFim textview com a hora de termino
     * @param sala edittext com a sala informada pelo usuário
     * @param Dia Strinf com o dia da semana
     * @return aula com os dados atualizados
     */
    public Aula preencheAula(Aula a, TextView tvInicio, TextView tvFim, EditText sala, String Dia) {
        int tempoNotificacao;
        if(notificar){
            tempoNotificacao = 60;
        }else{
            tempoNotificacao = 0;
        }
        String horaInicio;
        String horaFim;
        horaInicio = tvInicio.getText().toString();
        horaFim = tvFim.getText().toString();

        a.setDiaSemana(Dia);
        a.setSala(sala.getText().toString());

        a.setHora(horaInicio.substring(horaInicio.length()-5, horaInicio.length()));
        a.setHoraTermino(horaFim.substring(horaFim.length()-5, horaFim.length()));
        a.setTempoNotificacao(tempoNotificacao);
        return a;
    }

}
