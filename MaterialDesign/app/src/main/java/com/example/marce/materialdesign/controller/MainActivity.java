package com.example.marce.materialdesign.controller;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.dao.DataBaseDAO;
import com.example.marce.materialdesign.fragment.QuartaFeiraFragment;
import com.example.marce.materialdesign.fragment.QuintaFeiraFragment;
import com.example.marce.materialdesign.fragment.SabadoFragment;
import com.example.marce.materialdesign.fragment.SegundaFeiraFragment;
import com.example.marce.materialdesign.fragment.SextaFeiraFragment;
import com.example.marce.materialdesign.fragment.TercaFeiraFragment;
import com.example.marce.materialdesign.model.Aula;
import com.example.marce.materialdesign.service.TimeAlarm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FloatingActionButton floatingButton;

    private DataBaseDAO dao;
    private List<Aula> listaAulasBanco;
    private List<Aula> listaAulasSegundaFeira;
    private List<Aula> listaAulasTercaFeira;
    private List<Aula> listaAulasQuartaFeira;
    private List<Aula> listaAulasQuintaFeira;
    private List<Aula> listaAulasSextaFeira;
    private List<Aula> listaAulasSabado;




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        selecionarPaginaInicial(descobrirDiaDaSemana());
        getSupportActionBar().setIcon(R.mipmap.ic_launcher_relogio);
        dao = new DataBaseDAO(this);
        listaAulasBanco = new ArrayList<Aula>();
        listaAulasSegundaFeira = new ArrayList<Aula>();
        listaAulasTercaFeira = new ArrayList<Aula>();
        listaAulasQuartaFeira = new ArrayList<Aula>();
        listaAulasQuintaFeira = new ArrayList<Aula>();
        listaAulasSextaFeira = new ArrayList<Aula>();
        listaAulasSabado = new ArrayList<Aula>();
        floatingButton = (FloatingActionButton) findViewById(R.id.floatingAddButton);

        floatingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AdicaoActivity.class);
                startActivity(intent);
            }
        });
        popularLista();

    }

    /**
     * Método para iniciar os fragmentos e adicioná-los no ViewPageAdapter
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SegundaFeiraFragment(), "Seg");
        adapter.addFragment(new TercaFeiraFragment(), "Ter");
        adapter.addFragment(new QuartaFeiraFragment(), "Qua");
        adapter.addFragment(new QuintaFeiraFragment(), "Qui");
        adapter.addFragment(new SextaFeiraFragment(), "Sex");
        adapter.addFragment(new SabadoFragment(), "Sáb");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);

        }

        @Override
        public int getCount() {
            return mFragmentList.size();

        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onStart() {
        super.onStart();

        popularLista();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        popularLista();
    }

    /*
     * Método para popular as listas de cada dia da semana com as aulas do banco de dados.
     */
    public void popularLista() {

        limparLista();

        for (int i = 0; i < listaAulasBanco.size(); i++) {
            if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.segunda))) {
                listaAulasSegundaFeira.add(listaAulasBanco.get(i));
            } else if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.terca))) {
                listaAulasTercaFeira.add(listaAulasBanco.get(i));
            } else if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.quarta))) {
                listaAulasQuartaFeira.add(listaAulasBanco.get(i));
            } else if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.quinta))) {
                listaAulasQuintaFeira.add(listaAulasBanco.get(i));
            } else if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.sexta))) {
                listaAulasSextaFeira.add(listaAulasBanco.get(i));
            } else if (listaAulasBanco.get(i).getDiaSemana().equals(getString(R.string.sabado))) {
                listaAulasSabado.add(listaAulasBanco.get(i));
            }

        }
        ordernarLista();
    }

    /*
     * Método que ordena as listas dos dias da semana ao iniciar o aplicativo
     */
    public void ordernarLista() {

        listaAulasSegundaFeira = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasSegundaFeira);
        listaAulasTercaFeira = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasTercaFeira);
        listaAulasQuartaFeira = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasQuartaFeira);
        listaAulasQuintaFeira = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasQuintaFeira);
        listaAulasSextaFeira = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasSextaFeira);
        listaAulasSabado = MainPagerAdapter.ordenarDisciplinasHorario(listaAulasSabado);

    }

    /*
     * Método para limpar as listas com as aulas dos dias da semana
     */
    public void limparLista() {

        listaAulasBanco = dao.select();
        listaAulasSegundaFeira.clear();
        listaAulasTercaFeira.clear();
        listaAulasQuartaFeira.clear();
        listaAulasQuintaFeira.clear();
        listaAulasSextaFeira.clear();
        listaAulasSabado.clear();

    }

    /*
    * Método para descobrir o dia da semana atual do sistema
    */
    public int descobrirDiaDaSemana() {
        Calendar calendar = Calendar.getInstance();
        int dia = calendar.get(Calendar.DAY_OF_WEEK);
        return dia - 2;
    }

    /*
     * Método que inicia o aplicativo no dia da semana atual
     */
    public void selecionarPaginaInicial(int diaDaSemana) {
        tabLayout.setScrollPosition(diaDaSemana, 0f, true);
        viewPager.setCurrentItem(diaDaSemana);
    }

    public List<Aula> getListaAulasSegundaFeira() {
        return listaAulasSegundaFeira;
    }

    public List<Aula> getListaAulasTercaFeira() {
        return listaAulasTercaFeira;
    }

    public List<Aula> getListaAulasQuartaFeira() {
        return listaAulasQuartaFeira;
    }

    public List<Aula> getListaAulasQuintaFeira() {
        return listaAulasQuintaFeira;
    }

    public List<Aula> getListaAulasSextaFeira() {
        return listaAulasSextaFeira;
    }

    public List<Aula> getListaAulasSabado() {
        return listaAulasSabado;
    }


}

