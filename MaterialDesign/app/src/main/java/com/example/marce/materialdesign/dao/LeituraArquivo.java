package com.example.marce.materialdesign.dao;

import com.example.marce.materialdesign.model.Aula;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class LeituraArquivo {


    public List<Aula> leArquivosTxt() throws IOException { //  O BufferedReader lança tbm FileNotFoundException mas IOException e a super classe.
        Aula aula;
        String txt = null;
        BufferedReader leitura = new BufferedReader(new FileReader("../java/br/edu/unipampa/time2/apphorarios/Horarios.txt"));
        List<Aula> listaDeAulas = new ArrayList<Aula>();
        while ((txt = leitura.readLine()) != null) {
            String[] colunas = txt.split(",");
            //   aula = new Aula(colunas[6],colunas[1],colunas[4],colunas[9],colunas[2],separaCurso(colunas[3]),separaSemestre(colunas[3]),colunas[5]);
            //   listaDeAulas.add(aula);
        }
        leitura.close();
        return listaDeAulas;
    }

    //Separa semestre do curso
    public String[] separaSemestre(String texto) {
        String[] aux = texto.split(";");
        String[] semestre = new String[aux.length];
        String semestreAux;

        for (int cont = 0; cont < aux.length; cont++) {
            semestreAux = aux[cont].substring(2,4);
            semestre[cont] = semestreAux;
        }
        return semestre;
    }

    //Retira os semestre do curso
    public String[] separaCurso(String texto){
        String[] aux = texto.split(";|;\\s");
        String[] cursos = new String[aux.length];
        String cursoAux;

        for (int cont = 0; cont<aux.length;cont++){
            if(!aux[cont].contains("0")){
                cursos[cont] = aux[cont];
                continue;
            }
            cursoAux = aux[cont].substring(0,2);
            cursos[cont] = cursoAux;
        }
        return cursos;
    }
}