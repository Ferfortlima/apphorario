package com.example.marce.materialdesign.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.marce.materialdesign.fragment.QuartaFeiraFragment;
import com.example.marce.materialdesign.fragment.QuintaFeiraFragment;
import com.example.marce.materialdesign.fragment.SabadoFragment;
import com.example.marce.materialdesign.fragment.SegundaFeiraFragment;
import com.example.marce.materialdesign.fragment.SextaFeiraFragment;
import com.example.marce.materialdesign.fragment.TercaFeiraFragment;
import com.example.marce.materialdesign.model.Aula;

import java.util.ArrayList;
import java.util.List;


public class MainPagerAdapter extends FragmentPagerAdapter {


    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SegundaFeiraFragment();
            case 1:
                return new TercaFeiraFragment();
            case 2:
                return new QuartaFeiraFragment();
            case 3:
                return new QuintaFeiraFragment();
            case 4:
                return new SextaFeiraFragment();
            case 5:
                return new SabadoFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }

    /**
     * Método que ordena as listas dos dias da semana em ordem crescente pelo horário de início da aula
     * @param listaAulas
     * @return
     */
    public static ArrayList<Aula> ordenarDisciplinasHorario(List<Aula> listaAulas) {

        int cont = 0;
        ArrayList<String> horario = new ArrayList<String>();
        ArrayList<Aula> novaLista = new ArrayList<Aula>();

        // Adiciona os horários das disciplinas no vetor horário
        for (int i = 0; i < listaAulas.size(); i++) {
            horario.add(listaAulas.get(i).getHora());
        }
        //Ordenação dos horários
        java.util.Collections.sort(horario);
        //Adição dos horários ordenados na nova lista
        while (novaLista.size() < horario.size()) {
            for (int k = 0; k < listaAulas.size(); k++) {
                if (listaAulas.get(k).getHora() == horario.get(cont)) {
                    novaLista.add(listaAulas.get(k));
                    listaAulas.remove(k);
                    cont++;
                }
            }
        }

        return novaLista;
    }
}
