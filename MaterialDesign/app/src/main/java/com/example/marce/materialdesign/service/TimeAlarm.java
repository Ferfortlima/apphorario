package com.example.marce.materialdesign.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.controller.MainActivity;
import com.example.marce.materialdesign.model.Aula;

import java.util.Date;

public class TimeAlarm extends BroadcastReceiver
{
    Aula aula;

    public TimeAlarm() {}


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        this.aula = (Aula) bundle.getSerializable("aula");
        showNotification(context);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showNotification(Context context) {

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setWhen(System.currentTimeMillis())
                        .setSmallIcon(R.drawable.ic_action_hora)
                        .setContentTitle(aula.getNome())
                        .setContentText(aula.getHora() + " | " + aula.getSala());
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());

    }
}
