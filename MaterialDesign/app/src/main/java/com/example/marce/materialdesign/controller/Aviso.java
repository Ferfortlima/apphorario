package com.example.marce.materialdesign.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.dao.DataBaseDAO;
import com.example.marce.materialdesign.model.Aula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcus on 16/11/2016.
 */

public class Aviso {

    private Activity activity;

    public Aviso(Activity context){
        this.activity = context;
    }

    /*
    *   Método para exibir um alert dialog com título, mensagem e dois botões: Confirmar e Cancelar
    *   @param title Título do alert dialog
    *   @param message mensagem do alert dialog
    */
    public void showDialog(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        builder.setMessage(message).setTitle(null)
                .setCancelable(false)

                .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.finish();

                    }
                })
                .setNegativeButton(activity.getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
    *   Método para exibir um alert dialog com título, mensagem e um botão de OK
    *   @param message mensagem do alert dialog
    */
    public void ShowDialogInformacao(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        builder.setMessage(message).setTitle(null)
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
    *   Método para exibir um toast contendo uma mensagem
    *   @param message mensagem do toast
    */
    public void toastMessager(String mensagem){
        SingleToast.show(activity, mensagem, Toast.LENGTH_LONG);
    }

    /**
     * Classe para previnir que mais de um toast seja iniciado ao mesmo tempo
     */
    public static class SingleToast {

        private static Toast mToast;

        public static void show(Context context, String text, int duration) {
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(context, text, duration);
            mToast.show();
        }
    }

    /**
     * Método para exibir um alert dialog com opções de deletar apenas uma aula ou todas as aulas da disciplina
     * @param activityExclusao
     * @param message
     * @param aula
     * @param detalhesFragment
     */
    public void showDialogExclusao(final Activity activityExclusao, String message, final Aula aula, final Dialog detalhesFragment) {

        final String[] items = {activityExclusao.getString(R.string.excluir_um), activityExclusao.getString(R.string.excluir_todos)};
        final List<Integer> checkedItems = new ArrayList<>();

        final AlertDialog.Builder builder = new AlertDialog.Builder(activityExclusao);


        builder.setTitle(message)
                .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        checkedItems.clear();
                        checkedItems.add(item);
                    }

                })

                .setCancelable(false)

                .setPositiveButton(activityExclusao.getText(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DataBaseDAO dataBaseDAO = new DataBaseDAO(activityExclusao);
                        if(checkedItems.isEmpty()){
                            checkedItems.clear();
                            checkedItems.add(0);
                        }
                        if(checkedItems.get(0).equals(0)){
                            dataBaseDAO.delete(aula);
                            Toast.makeText(activityExclusao.getBaseContext(), activityExclusao.getString(R.string.exclusao_aula_sucesso),
                                    Toast.LENGTH_LONG).show();
                            detalhesFragment.dismiss();
                            activityExclusao.recreate();
                        }else{
                            dataBaseDAO.deleteAll(aula);
                            Toast.makeText(activityExclusao.getBaseContext(), activityExclusao.getString(R.string.exclusao_aulas_sucesso),
                                    Toast.LENGTH_LONG).show();
                            detalhesFragment.dismiss();
                            activityExclusao.recreate();
                        }

                    }
                })
                .setNegativeButton(activityExclusao.getString(R.string.cancelar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();

        alert.show();

    }
}
