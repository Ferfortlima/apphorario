package com.example.marce.materialdesign.controller;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.dao.DataBaseDAO;
import com.example.marce.materialdesign.fragment.DetalhesDisciplinaFragment;
import com.example.marce.materialdesign.model.Aula;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListAdapterAula extends ArrayAdapter<Aula> {

    private final Context context;
    private final List<Aula> values;
    private DataBaseDAO dao;
    private List<Aula> listaAulasBanco = new ArrayList<>();
    private View rowView;

    public ListAdapterAula(List<Aula> aulas, Context ctx) {
        super(ctx, 0, aulas);
        this.values = aulas;
        this.context = ctx;
        dao = new DataBaseDAO(ctx);
        listaAulasBanco = dao.select();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // verifica se há conflitos
        TextView horario;
        if(temConflitoHora(listaAulasBanco, values.get(position))){
            rowView = inflater.inflate(R.layout.list_adapter_aula_conflito, parent, false);
            horario = (TextView) rowView.findViewById(R.id.hora_com_conflito);
        }else {
            rowView = inflater.inflate(R.layout.list_adapter_aula, parent, false);
            horario = (TextView) rowView.findViewById(R.id.hora_sem_conflito);
        }


        TextView nome = (TextView) rowView.findViewById(R.id.disc);
        TextView sala = (TextView) rowView.findViewById(R.id.sala);
        TextView professor = (TextView) rowView.findViewById(R.id.professor);
        TextView turma = (TextView) rowView.findViewById(R.id.turma);


        horario.setText(values.get(position).getHora() + " " + values.get(position).getHoraTermino());
        nome.setText(values.get(position).getNome());
        turma.setText(values.get(position).getTurma());
        professor.setText(values.get(position).getProfessor());
        sala.setText(values.get(position).getSala());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.support.v4.app.FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();

                DetalhesDisciplinaFragment dFragment = new DetalhesDisciplinaFragment();

                Bundle args = new Bundle();

                Aula aula = values.get(position);

                args.putSerializable("aula",aula);

                dFragment.setArguments(args);

                dFragment.show(ft, "Dialog Fragment");

            }
        });

        return rowView;

    }

    /**
     *  Método para verificar se uma aula2, pertencente a listaInsert, tem conflito de horário com as aulas já existentes
     * @param listaAulasBanco lista de aulas existentes no banco
     * @param aula Aula a ser comparada
     * @return boolean true se ocorreu conflito, senão false
     */
    public boolean temConflitoHora(List<Aula> listaAulasBanco, Aula aula){
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        try {
            for (int i = 0; i < listaAulasBanco.size(); i++) {
                if (listaAulasBanco.get(i).getDiaSemana().equals(aula.getDiaSemana())) {

                    Date aula1Inicio = parser.parse(listaAulasBanco.get(i).getHora());
                    Date aula1Final = parser.parse(listaAulasBanco.get(i).getHoraTermino());
                    Date aula2Inicio = parser.parse(aula.getHora());
                    Date aula2Final = parser.parse(aula.getHoraTermino());

                    if(listaAulasBanco.get(i).getId() != aula.getId() ) {

                        if (aula2Inicio.after(aula1Inicio) && aula2Inicio.before(aula1Final)  ||
                                aula2Final.after(aula1Inicio) && aula2Final.before(aula1Final)||
                                aula1Inicio.equals(aula2Inicio) || aula1Final.equals(aula2Final)) {
                            //deu conflito
                            return true;
                        }


                        if (aula1Inicio.after(aula2Inicio) && aula1Inicio.before(aula2Final) ||
                                aula1Final.after(aula2Inicio) && aula1Final.before(aula2Final)||
                                aula2Inicio.equals(aula1Inicio) || aula2Final.equals(aula1Final)) {
                            //deu conflito
                            return true;
                        }
                    }
                }
            }

        } catch (ParseException e) {
            //Data invalida
        }
        return false;
    }

}




