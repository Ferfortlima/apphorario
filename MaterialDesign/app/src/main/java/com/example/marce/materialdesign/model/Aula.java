package com.example.marce.materialdesign.model;
import android.content.Context;
import java.io.Serializable;

/**
 * Created by marce on 07/10/2016.
 */

public class Aula implements Serializable{

    private String nome;
    private String professor;
    private String horaInicio;
    private String horaTermino;
    private String sala;
    private String turma;
    private String curso;
    private String semestre;
    private String diaSemana;
    private int id;
    private int tempoNotificacao;// em minutos (0 Não tem notificação)

    public Aula(){}

    public Aula(Context ctx, int i, Aula aula){

    }


    public Aula(String nome, String professor, String hora, String turma, String sala, String curso ,String semestre,String diaSemana,int id, String horaTermino,int tempo) {
        this.nome = nome;
        this.professor = professor;
        this.horaInicio = hora;
        this.turma = turma;
        this.sala = sala;
        this.curso = curso;
        this.semestre = semestre;
        this.diaSemana = diaSemana;
        this.horaTermino = horaTermino;
        this.tempoNotificacao = tempo;
        this.setId(id);
    }

    public void setTempoNotificacao(int tempo){
        this.tempoNotificacao = tempo;
    }

    public int getTempoNotificacao(){
        return this.tempoNotificacao;
    }

    public void setCurso(String curso){
        this.curso = curso;
    }

    public String getCurso(){
        return curso;
    }

    public void setSemestre(String semestre){
        this.semestre = semestre;
    }

    public String getSemestre() {
        return semestre;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getHora() {
        return horaInicio;
    }

    public void setHora(String hora) {
        this.horaInicio = hora;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }

    @Override
    public String toString() {
        return "Aula{" +
                "nome='" + nome + '\'' +
                ", professor='" + professor + '\'' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaTermino='" + horaTermino + '\'' +
                ", sala='" + sala + '\'' +
                ", turma='" + turma + '\'' +
                ", curso='" + curso + '\'' +
                ", semestre='" + semestre + '\'' +
                ", diaSemana='" + diaSemana + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //metodos para receber somente a hora ex: "18" e somente os minutos ex: "30"
   public int getHorasNotificacao(){
       String[] time = horaInicio.split ( ":" );
       int hora = Integer.parseInt ( time[0].trim() );
       return hora;
   }
   public int getMinutosNotificacao(){
       String[] time = horaInicio.split ( ":" );
       int minutos = Integer.parseInt ( time[1].trim() );
       return minutos;
   }


}
