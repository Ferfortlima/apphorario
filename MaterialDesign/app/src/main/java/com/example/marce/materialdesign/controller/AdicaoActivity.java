package com.example.marce.materialdesign.controller;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.marce.materialdesign.R;
import com.example.marce.materialdesign.dao.DataBaseDAO;
import com.example.marce.materialdesign.model.Aula;
import com.example.marce.materialdesign.service.TimeAlarm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AdicaoActivity extends AppCompatActivity {

    private Aula aula = new Aula();
    private List<Aula> diaSalaHora = new ArrayList<>();
    private List<Aula> aulas = new ArrayList<>();
    private int ID_Notificacao;
    DataBaseDAO dao = new DataBaseDAO(this);
    Aviso aviso = new Aviso(AdicaoActivity.this);

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    // variáveis que representam se os checkbox's estão selecionados
    private boolean booleanSegunda;
    private boolean booleanTerca;
    private boolean booleanQuarta;
    private boolean booleanQuinta;
    private boolean booleanSexta;
    private boolean booleanSabado;
    private boolean notificar;

    Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicao);
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //não entrar na tela já com o teclado aberto
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_adicao, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (isAlgoPreenchido()){
                    aviso.showDialog(getString(R.string.descarte_alteracao));
                }else{
                    finish();
                }
                return true;
            case R.id.action_salvar:
                    salvarButton();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (isAlgoPreenchido()){
            aviso.showDialog(getString(R.string.descarte_alteracao));
        }else{
            finish();
        }
    }

    /**
     * Método para checar se a mensagem de descartar alterações deve ser exibida ou não
     * @return boolean true se algo foi preenchido e false se nada foio preenchido
     */

    private boolean isAlgoPreenchido() {

        TextInputLayout nome = (TextInputLayout) findViewById(R.id.textInputLayout_nome);
        //TextInputLayout professor = (TextInputLayout) findViewById(R.id.textInputLayout_profesor);
        //TextInputLayout curso = (TextInputLayout) findViewById(R.id.textInputLayout_curso);
        //TextInputLayout turma = (TextInputLayout) findViewById(R.id.textInputLayout_turma);
        //TextInputLayout semestre = (TextInputLayout) findViewById(R.id.textInputLayout_semestre);


        if(verificaCheckBoxes()){
            return true;
        }else{
            if (nome.getEditText().getText().toString().trim().length() > 0) {
                return true;
            }
//            if (turma.getEditText().getText().toString().trim().length() > 0) {
//                return true;
//            }
//            if (professor.getEditText().getText().toString().trim().length() > 0) {
//                return true;
//            }
//            if (curso.getEditText().getText().toString().trim().length() > 0) {
//                return true;
//            }
//            if (semestre.getEditText().getText().toString().trim().length() > 0) {
//                return true;
//            }
//            if (turma.getEditText().getText().toString().trim().length() > 0) {
//                return true;
//            }
        }
        return false;
    }



    /*
    *   Método para receber os dados informados pelo usuário e salvar em um objeto Aula;
    */
    public void getInfoAula() {
        EditText nome = (EditText) findViewById(R.id.textInput_nome);
        EditText professor = (EditText) findViewById(R.id.textInput_professor);
        EditText curso = (EditText) findViewById(R.id.textInput_curso);
        EditText turma = (EditText) findViewById(R.id.textInput_turma);
        EditText semestre = (EditText) findViewById(R.id.textInput_semestre);

        aula.setNome(nome.getText().toString());
        aula.setProfessor(professor.getText().toString());
        aula.setSemestre(semestre.getText().toString());
        aula.setCurso(curso.getText().toString());
        aula.setTurma(turma.getText().toString());
    }

    /*
    *   Método para adicionar todos os dias selecionados juntamente com seus respectivos campos sala e hora
    * em uma lista de Aula
    */
    public void getDiaSalaHora(boolean segunda, boolean terca, boolean quarta, boolean quinta, boolean sexta, boolean sabado) {
        diaSalaHora.clear();
        TextView tvInicio;
        TextView tvFim;
        Aula a;

        if (segunda) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_segunda);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_segunda);
            tvFim = (TextView) findViewById(R.id.hora_fim_segunda);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.segunda));
            diaSalaHora.add(a);
        }
        if (terca) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_terca);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_terca);
            tvFim = (TextView) findViewById(R.id.hora_fim_terca);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.terca));
            diaSalaHora.add(a);
        }
        if (quarta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_quarta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quarta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quarta);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.quarta));
            diaSalaHora.add(a);
        }
        if (quinta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_quinta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quinta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quinta);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.quinta));
            diaSalaHora.add(a);
        }
        if (sexta) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_sexta);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sexta);
            tvFim = (TextView) findViewById(R.id.hora_fim_sexta);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.sexta));
            diaSalaHora.add(a);
        }
        if (sabado) {
            a = new Aula();
            EditText sala = (EditText) findViewById(R.id.sala_sabado);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sabado);
            tvFim = (TextView) findViewById(R.id.hora_fim_sabado);
            a = preencherAula(a,tvInicio, tvFim, sala, getString(R.string.sabado));
            diaSalaHora.add(a);
        }
    }


    /**
     * Método que insere os dados informados pelo usuário em um objeto aula
     * @param a  Objeto Aula a ser preenchido
     * @param tvInicio textview com a hora de inicio
     * @param tvFim textview com a hora de termino
     * @param sala edittext com a sala informada pelo usuário
     * @param Dia Strinf com o dia da semana
     * @return aula com os dados atualizados
     */
    public Aula preencherAula(Aula a, TextView tvInicio, TextView tvFim, EditText sala, String Dia){
        String horaInicio;
        String horaFim;
        horaInicio = tvInicio.getText().toString();
        horaFim = tvFim.getText().toString();

        a.setDiaSemana(Dia);
        a.setSala(sala.getText().toString());
        a.setHora(horaInicio.substring(horaInicio.length()-5, horaInicio.length()));
        a.setHoraTermino(horaFim.substring(horaFim.length()-5, horaFim.length()));

        return a;
    }


    /*
     *  Combinar as informações salvas no objeto Aula com as informações salvas na Lista de Aula contendo
     *a sala, hora e dia de cada aula
     */
    public void salvarButton() {
        //Habilita o receiver
        ComponentName receiver = new ComponentName(this, TimeAlarm.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        boolean conflito = false;
        aulas.clear();
        //getNotificar();
        getInfoAula();
        getDiaSalaHora(booleanSegunda, booleanTerca, booleanQuarta, booleanQuinta, booleanSexta, booleanSabado);

        if (verificarCampos()) {
            int cont = diaSalaHora.size();

            for (int i = 0; i < cont; i++) {
                String dia = diaSalaHora.get(i).getDiaSemana();
                String sala = diaSalaHora.get(i).getSala();
                String hora = diaSalaHora.get(i).getHora();
                String horaFim = diaSalaHora.get(i).getHoraTermino();
                addAulaCompleta(aula, dia, sala, hora, horaFim, notificar);
            }

            List<Aula> listaAulasBanco = dao.select();

            //Verifica se há conflito de horário e insere no banco
            for (Aula a : aulas) {
                dao.insert(a);

                alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, TimeAlarm.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("aula", a);
                intent.putExtras(bundle);

                alarmIntent = PendingIntent.getBroadcast(this, ID_Notificacao, intent, 0);
                ID_Notificacao++;
                // define a hora e dia da notificação
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.DAY_OF_WEEK, encontrarDiaNotificacao(a.getDiaSemana()));
                calendar.set(Calendar.HOUR_OF_DAY, a.getHorasNotificacao());
                calendar.set(Calendar.MINUTE, a.getMinutosNotificacao());
                calendar.set(Calendar.SECOND, 0);
                // setRepeating() permite especificar um preciso intervalo de tempo, neste caso
                // uma semana  AlarmManager.INTERVAL_DAY*7
                alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY*7, alarmIntent);
                //sendBroadcast(intent);

                if (existeNoBanco(listaAulasBanco, aulas) || temConflitoHora(listaAulasBanco, aulas)) {
                    conflito = true;
                }
            }

            if(conflito){
                aviso.toastMessager(getString(R.string.adicao_discilpina_conflito));
            }else {
                aviso.toastMessager(getString(R.string.adicao_disciplina_sucesso));
            }





            finish();
        } else {
            //se campos nao forem preenchidos erros aparecem na tela
        }
    }


    /*
     * Método para verificar se todos os campos básicos foram preenchidos, e no mínimo um checkbox selecionado
     * e seus campos foram preenchidos, caso contrario seta uma mensagem de erro no campo a ser preenchido
     * @return TRUE se tudo estiver preenchido corretamente, caso contrário FALSE
     */
    public boolean verificarCampos() {
        //verifica se o nome da disciplina foi informado
        if (verificarCamposDisciplina()) {

            if (verificarHorasSalas()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * Método para setar os erros nos campos básicos da disciplina
     * @return boolean retorna TRUE se tudo estiver preenchido corretamente, caso contrário FALSE
     */
    public boolean verificarCamposDisciplina() {
        boolean tudook = true;

        TextInputLayout nome = (TextInputLayout) findViewById(R.id.textInputLayout_nome);

//        TextInputLayout professor = (TextInputLayout) findViewById(R.id.textInputLayout_profesor);
//        TextInputLayout curso = (TextInputLayout) findViewById(R.id.textInputLayout_curso);
//        TextInputLayout turma = (TextInputLayout) findViewById(R.id.textInputLayout_turma);
//        TextInputLayout semestre = (TextInputLayout) findViewById(R.id.textInputLayout_semestre);
//        TextInputLayout dia = (TextInputLayout) findViewById(R.id.textInputLayout_dias);
//
//        if (!verificaCheckBoxes()) {
//            dia.setErrorEnabled(true);
//            dia.setError(getString(R.string.escolha_um_dia));
//            dia.requestFocus();
//            tudook = false;
//        }
//
//        if (turma.getEditText().getText().toString().trim().length() == 0) {
//            turma.setErrorEnabled(true);
//            turma.setError(getString(R.string.campo_vazio));
//            turma.requestFocus();
//            tudook = false;
//        } else {
//            turma.setError(null);
//        }
//
//        if (semestre.getEditText().getText().toString().trim().length() == 0) {
//            semestre.setErrorEnabled(true);
//            semestre.setError(getString(R.string.campo_vazio));
//            tudook = false;
//            semestre.requestFocus();
//        } else {
//            semestre.setError(null);
//        }
//
//        if (curso.getEditText().getText().toString().trim().length() == 0) {
//            curso.setErrorEnabled(true);
//            curso.setError(getString(R.string.campo_vazio));
//            curso.requestFocus();
//            tudook = false;
//        } else {
//            curso.setError(null);
//        }
//
//        if (professor.getEditText().getText().toString().trim().length() == 0) {
//            professor.setErrorEnabled(true);
//            professor.setError(getString(R.string.campo_vazio));
//            tudook = false;
//            professor.requestFocus();
//        } else {
//            professor.setError(null);
//        }

        if (nome.getEditText().getText().toString().trim().length() == 0) {
            nome.setErrorEnabled(true);
            nome.setError(getString(R.string.campo_vazio));
            tudook = false;
            nome.requestFocus();
        } else {
            nome.setError(null);
        }
        return tudook;
    }

    /*
     * Método para setar os erros nos campos de salas e horas que estiverem incorretos
     * @return boolean retorna TRUE se tudo estiver preenchido corretamente, caso contrário FALSE
     */
    public boolean verificarHorasSalas() {

        boolean tudook = true;
        boolean avisoHora = false;
        TextView tvInicio;
        TextView tvFim;
        String horaInicio;
        String horaFim;

        if(!verificaCheckBoxes()){
            tudook = false;
            return tudook;
        }

        if (booleanSegunda) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaseg);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_segunda);
            tvFim = (TextView) findViewById(R.id.hora_fim_segunda);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }
            if(!verificaHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanTerca) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salater);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_terca);
            tvFim = (TextView) findViewById(R.id.hora_fim_terca);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }else
              if(!verificaHora(horaInicio,horaFim, tvInicio)){
                    sala.setError(null);
                    tudook = false;
                    avisoHora = true;
              }
        }
        if (booleanQuarta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaqua);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quarta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quarta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }else
            if (!verificaHora(horaInicio, horaFim, tvInicio)) {
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanQuinta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salaqui);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_quinta);
            tvFim = (TextView) findViewById(R.id.hora_fim_quinta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }else
 if(!verificaHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanSexta) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salasex);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sexta);
            tvFim = (TextView) findViewById(R.id.hora_fim_sexta);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }else
            if(!verificaHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if (booleanSabado) {
            TextInputLayout sala = (TextInputLayout) findViewById(R.id.textInputLayout_salasab);
            tvInicio = (TextView) findViewById(R.id.hora_inicio_sabado);
            tvFim = (TextView) findViewById(R.id.hora_fim_sabado);
            horaInicio = tvInicio.getText().toString();
            horaInicio = horaInicio.substring(horaInicio.length()-5, horaInicio.length());
            horaFim = tvFim.getText().toString();
            horaFim = horaFim.substring(horaFim.length()-5, horaFim.length());
//            if (!verificaSala(sala)) {
//                tudook = false;
//                return tudook;
//            }else
            if(!verificaHora(horaInicio,horaFim, tvInicio)){
                sala.setError(null);
                tudook = false;
                avisoHora = true;
            }
        }
        if(avisoHora) {
            aviso.ShowDialogInformacao(getString(R.string.alterar_horario_inicio));
        }
        return tudook;
    }

    /*
     * Método para verificar se pelo menos um dos checkboxes foi marcado
     */
    public boolean verificaCheckBoxes() {

        CheckBox segunda = (CheckBox) findViewById(R.id.checkBox_segunda);
        CheckBox terca = (CheckBox) findViewById(R.id.checkBox_terca);
        CheckBox quarta = (CheckBox) findViewById(R.id.checkBox_quarta);
        CheckBox quinta = (CheckBox) findViewById(R.id.checkBox_quinta);
        CheckBox sexta = (CheckBox) findViewById(R.id.checkBox_sexta);
        CheckBox sabado = (CheckBox) findViewById(R.id.checkBox_sabado);


        boolean tudook = false;

        if (segunda.isChecked()) {
            tudook = true;
        }
        if (terca.isChecked()) {
            tudook = true;
        }
        if (quarta.isChecked()) {
            tudook = true;
        }
        if (quinta.isChecked()) {
            tudook = true;
        }
        if (sexta.isChecked()) {
            tudook = true;
        }
        if (sabado.isChecked()) {
            tudook = true;
        }

        return tudook;
    }


    /*
     * Método para monitorar os checkboxes e mudar a visibilidade dos layouts de acordo com o
     * que foi selecionado
     */
    public void onCheckboxClicked(View view) {
        TextInputLayout dia = (TextInputLayout) findViewById(R.id.textInputLayout_dias);
        boolean checked = ((CheckBox) view).isChecked();

        LinearLayout AulaSegunda = (LinearLayout) findViewById(R.id.layout_aula_segunda);
        LinearLayout AulaTerca = (LinearLayout) findViewById(R.id.layout_aula_terca);
        LinearLayout AulaQuarta = (LinearLayout) findViewById(R.id.layout_aula_quarta);
        LinearLayout AulaQuinta = (LinearLayout) findViewById(R.id.layout_aula_quinta);
        LinearLayout AulaSexta = (LinearLayout) findViewById(R.id.layout_aula_sexta);
        LinearLayout AulaSabado = (LinearLayout) findViewById(R.id.layout_aula_sabado);

        dia.requestFocus();

        switch (view.getId()) {
            case R.id.checkBox_segunda:
                if (checked) {
                    booleanSegunda = true;
                    dia.setError(null);
                    AulaSegunda.setVisibility(View.VISIBLE);
                } else {
                    booleanSegunda = false;
                    AulaSegunda.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_terca:
                if (checked) {
                    booleanTerca = true;
                    dia.setError(null);
                    AulaTerca.setVisibility(View.VISIBLE);
                } else {
                    booleanTerca = false;
                    AulaTerca.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_quarta:
                if (checked) {
                    booleanQuarta = true;
                    dia.setError(null);
                    AulaQuarta.setVisibility(View.VISIBLE);
                } else {
                    booleanQuarta = false;
                    AulaQuarta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_quinta:
                if (checked) {
                    booleanQuinta = true;
                    dia.setError(null);
                    AulaQuinta.setVisibility(View.VISIBLE);
                } else {
                    booleanQuinta = false;
                    AulaQuinta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_sexta:
                if (checked) {
                    booleanSexta = true;
                    dia.setError(null);
                    AulaSexta.setVisibility(View.VISIBLE);
                } else {
                    booleanSexta = false;
                    AulaSexta.setVisibility(View.GONE);
                }
                break;
            case R.id.checkBox_sabado:
                if (checked) {
                    booleanSabado = true;
                    dia.setError(null);
                    AulaSabado.setVisibility(View.VISIBLE);
                } else {
                    booleanSabado = false;
                    AulaSabado.setVisibility(View.GONE);
                }
                break;
        }
    }


    // Método que verifica se o usuário selecionou uma hora válida
    public boolean verificaHora(String horaInicio, String horaFinal, TextView tv) {
        boolean tudook = true;

        //verifica se a hora de término é maior que a de inicio
        List<String> listhoras = new ArrayList<>();
        listhoras.add(horaInicio);
        listhoras.add(horaFinal);
        Collections.sort(listhoras);
        if(listhoras.get(0).equals(horaFinal)){
            tv.setTextColor(Color.RED);
            tv.requestFocus();
            tudook = false;
        }

        if(tudook){
            tv.setTextColor(Color.BLACK);
            tv.clearFocus();
        }
        return tudook;
    }

    // Método que verifica se o usuário informou uma sala válida (não deixou o campo em branco)
    public boolean verificaSala(TextInputLayout sala) {
        boolean tudook = true;
        if (sala.getEditText().getText().toString().trim().length() == 0) {
            sala.setErrorEnabled(true);
            sala.setError(getString(R.string.campo_vazio));
            tudook = false;
            sala.requestFocus();
        }else {
            sala.setError(null);
            sala.clearFocus();
        }
        return tudook;
    }


    /*
    *  Método para unir as informações das duas telas em uma lista de objetos Aula de acordo com a quantidade de dias selecionados
    *  @param aula objeto aula preenchido utilizando edittext
    *  @param dia dia dia informado nos checkboxes
    *  @param sala sala correspondente ao dia daquela aula
    *  @param hora hora correspondente ao  dia daquela aula
    *  @param notificar boolean, variavel que armazena se o usuário que ou não receber notificações para esta disciplina
    */
    public void addAulaCompleta(Aula aula, String dia, String sala, String hora, String horaFim, boolean notificar) {
        Aula aulacompleta = new Aula();

        aulacompleta.setNome(aula.getNome());
        aulacompleta.setTurma(aula.getTurma());
        aulacompleta.setProfessor(aula.getProfessor());
        aulacompleta.setCurso(aula.getCurso());
        aulacompleta.setSemestre(aula.getSemestre());

        aulacompleta.setDiaSemana(dia);
        aulacompleta.setSala(sala);
        aulacompleta.setHora(hora);
        aulacompleta.setHoraTermino(horaFim);
        if(notificar){
            aulacompleta.setTempoNotificacao(60);
        }else{
            aulacompleta.setTempoNotificacao(0);
        }

        aulas.add(aulacompleta);
    }

    /*
     * Método para verificar se há conflitos de horários, recebendo todas as disciplinas do aluno salvas no banco de dados e
     * as aulas que o aluno deseja cadastrar
     */
    public boolean existeNoBanco(List<Aula> listaAulasBanco, List<Aula> listaInsert) {

        boolean retorno = false;


        for (int i = 0; i < listaAulasBanco.size(); i++) {
            for (int j = 0; j < listaInsert.size(); j++) {
                if (listaAulasBanco.get(i).getDiaSemana().equals(listaInsert.get(j).getDiaSemana())
                        &&
                    listaAulasBanco.get(i).getHora().equals(listaInsert.get(j).getHora())) {
                    retorno = true;
                    break;
                }
            }
        }
        return retorno;
    }


    /**
     *  Método para verificar se uma aula2, pertencente a listaInsert, começa durante o período de uma aula1
     * que pertence a listaAulasBanco
     * @param listaAulasBanco lista de aulas existentes no banco
     * @param listaInsert lista de aulas a serem inseridas
     * @return boolean true se ocorreu conflito, senão false
     */
    public boolean temConflitoHora(List<Aula> listaAulasBanco, List<Aula> listaInsert){
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        try {

            for (int i = 0; i < listaAulasBanco.size(); i++) {
                for (int j = 0; j < listaInsert.size(); j++) {
                    if (listaAulasBanco.get(i).getDiaSemana().equals(listaInsert.get(j).getDiaSemana())) {

                        Date aula1Inicio = parser.parse(listaAulasBanco.get(i).getHora());
                        Date aula1Final = parser.parse(listaAulasBanco.get(i).getHoraTermino());
                        Date aula2Inicio = parser.parse(listaInsert.get(j).getHora());

                        if (aula2Inicio.after(aula1Inicio) && aula2Inicio.before(aula1Final)) {
                            //deu conflito
                            return true;
                        }
                    }
                }
            }

        } catch (ParseException e) {
            //Data invalida
        }
        return false;
    }

    /**
     * Método para inflar o timepickerdialog correspondente ao dia e horario(inicial ou final) selecionado
     * @param v view que contem as tags
     */
    public void ShowTimepicker(View v){
        String dia = (String)v.getTag();
        DialogFragment newFragment;
        switch (dia){
            case "segundainicio":
                newFragment = new TimePickerFragment("hora_inicio_segunda", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "tercainicio":
                newFragment = new TimePickerFragment("hora_inicio_terca", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "quartainicio":
                newFragment = new TimePickerFragment("hora_inicio_quarta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "quintainicio":
                newFragment = new TimePickerFragment("hora_inicio_quinta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "sextainicio":
                newFragment = new TimePickerFragment("hora_inicio_sexta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "sabadoinicio":
                newFragment = new TimePickerFragment("hora_inicio_sabado", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;


            case "segundafim":
                newFragment = new TimePickerFragment("hora_fim_segunda", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "tercafim":
                newFragment = new TimePickerFragment("hora_fim_terca", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "quartafim":
                newFragment = new TimePickerFragment("hora_fim_quarta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "quintafim":
                newFragment = new TimePickerFragment("hora_fim_quinta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "sextafim":
                newFragment = new TimePickerFragment("hora_fim_sexta", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;
            case "sabadofim":
                newFragment = new TimePickerFragment("hora_fim_sabado", getPackageName());
                newFragment.show(getFragmentManager(),"TimePicker");
                break;

            default:
                break;
        }

    }

    /**
     * Agendar a notificação de uma determinada aula
     * @param aula objeto com todas as informações necessárias para agendar a notificação em um determinado
     *             dia e horário
     */
    public void agendarNotificacao(Aula aula){


    }

    /**
     * Verifica em qual dia ocorre aquela aula para agendar a notificação
     * @param dia dia da aula
     * @return interiro correspondente ao dia da semana
     */
    public int encontrarDiaNotificacao(String dia){

        if(dia.equals(getString(R.string.segunda))){
            return Calendar.MONDAY;
        }
        if(dia.equals(getString(R.string.terca))){
            return Calendar.TUESDAY;
        }
        if(dia.equals(getString(R.string.quarta))){
            return Calendar.WEDNESDAY;
        }
        if(dia.equals(getString(R.string.quinta))){
            return Calendar.THURSDAY;
        }
        if(dia.equals(getString(R.string.sexta))){
            return Calendar.FRIDAY;
        }
        if(dia.equals(getString(R.string.sabado))){
            return Calendar.SATURDAY;
        }

        return 0;
    }


    /**
     * Classe para gerenciar a ação do timepicker
     */
    @SuppressLint("ValidFragment")
    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        private String id;
        private String packageName;

        public TimePickerFragment(){

        }
        public TimePickerFragment(String id, String packageName) {
            this.id = id;
            this.packageName = packageName;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            //Use the current time as the default values for the time picker
            final Calendar c = Calendar.getInstance();
            int hora = c.get(Calendar.HOUR_OF_DAY);
            int minuto = c.get(Calendar.MINUTE);

            //Create and return a new instance of TimePickerDialog
            return new TimePickerDialog(getActivity(),this, hora, minuto, DateFormat.is24HourFormat(getActivity()));
        }

        //onTimeSet() callback method
        public void onTimeSet(TimePicker view, int hora, int minuto){
            String horaAux = new String();

            //busca o textview a ser editado com o horário informado
            int resID = getResources().getIdentifier(id, "id", packageName);
            TextView tv = (TextView) getActivity().findViewById(resID);
            //Altera o valor do horário de inicio com o valor informado
            horaAux = tv.getText().toString();
            horaAux = horaAux.substring(0,horaAux.length()-5);
            tv.setText(horaAux + String.format("%02d:%02d", hora, minuto));
        }
    }

}