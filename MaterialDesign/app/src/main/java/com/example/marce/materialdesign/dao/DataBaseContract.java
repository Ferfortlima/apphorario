package com.example.marce.materialdesign.dao;

import android.provider.BaseColumns;


public class DataBaseContract {

    public static abstract class Horario implements BaseColumns {
        public static final String NOME_TABELA = "Horario";
        public static final String COLUNA_HORARIO_INICIO = "horarioInicio";
        public static final String COLUNA_HORARIO_FINAL = "horarioFinal";
    }

    public static abstract class Dia implements BaseColumns {
        public static final String NOME_TABELA = "Dia";
        public static final String COLUNA_DIA = "dia";
    }

    public static abstract class Sala implements BaseColumns {
        public static final String NOME_TABELA = "Sala";
        public static final String COLUNA_SALA = "sala";
    }

    public static abstract class Turma implements BaseColumns {
        public static final String NOME_TABELA = "Turma";
        public static final String COLUNA_TURMA = "turma";
    }

    public static abstract class Curso implements BaseColumns {
        public static final String NOME_TABELA = "Curso";
        public static final String COLUNA_CURSO = "nomeCurso";
    }

    public static abstract class Professor implements BaseColumns {
        public static final String NOME_TABELA = "Professor";
        public static final String COLUNA_PROFESSOR = "nomeProfessor";
    }

    public static abstract class Aula implements BaseColumns {
        public static final String NOME_TABELA = "Aula";
        public static final String COLUNA_FK_SALA = "fkSala";
        public static final String COLUNA_FK_DIA = "fkDia";
        public static final String COLUNA_FK_HORARIO = "fkHorario";

    }

    public static abstract class Disciplina implements BaseColumns {
        public static final String NOME_TABELA = "Disciplina";
        public static final String COLUNA_NOME_DISCIPLINA = "nomeDisciplina";
        public static final String COLUNA_SEMESTRE = "Semestre";
        public static final String COLUNA_NOTIFICACAO = "tempoNotificacao";
        public static final String COLUNA_FK_TURMA = "fkTurma";
        public static final String COLUNA_FK_CURSO = "fkCurso";
        public static final String COLUNA_FK_PROFESSOR = "fkProfessor";

    }

    public static abstract class Aluno implements BaseColumns {
        public static final String NOME_TABELA = "Aluno";
        public static final String COLUNA_FK_AULA_HAS_DISCIPLINA = "fk_Aula_has_disciplinas";

    }

    public static abstract class Aula_has_disciplinas implements BaseColumns{
        public static final String NOME_TABELA = "Aula_has_disciplinas";
        public static final String FK_AULA = "fkAula";
        public static final String FK_DISCIPLINA = "fkDisciplina";

    }


}
